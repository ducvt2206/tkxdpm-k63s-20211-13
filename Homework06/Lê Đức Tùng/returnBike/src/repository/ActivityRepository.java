package repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import common.enums.StatusActivity;
import entity.Activity;
import java.sql.ResultSet;
public class ActivityRepository extends BaseRepository {

	public ActivityRepository() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean insertActivity(int id) throws Exception {
		try {
			PreparedStatement stat = connection.prepareStatement(
					"Insert into activity(status, rent_time, back_time, bike_id) values (?, ?, ?, ?) ");
			stat.setString(1, StatusActivity.RENTING.getStatus());
			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			stat.setTimestamp(3, null);
			stat.setInt(4, id);
			stat.execute();
			return true;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
   
	public static Activity findActivityById(int id) throws SQLException{
		Activity act = new Activity();
		String query = "SELECT * FROM" + "Acitivity" + "WHERE" + "id = " + Integer.toString(id) + ";";
		ResultSet res = Query(query);
		act.setBackTime(res.getTimestamp("rent_time"));
		act.setBikeId(res.getInt("bike_id"));
		act.setAccountId(res.getInt("account_id"));
		act.setDepositMoney(res.getString("depositMoney"));
		act.setId(res.getInt("id"));
		return act;
	}
	
	public static void updateActivityWithBackTime(Timestamp time) {
		
	}
	
//	public boolean updateActivity(int id, Activity activity) throws Exception {
//		try {
//			PreparedStatement stat = connection.prepareStatement(
//					"Insert into activity(status, rent_time, back_time, bike_id) values (?, ?, ?, ?) ");
//			stat.setString(1, activity.getStatus());
//			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
//			stat.setTimestamp(3, null);
//			stat.setInt(4, activity.getBikeId());
//			stat.execute();
//			return true;
//		} catch (Exception e) {
//			throw new Exception(e.getMessage());
//		}
//	}
}
