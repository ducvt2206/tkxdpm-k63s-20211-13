package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Station;
import entity.StationSlot;


public class StationRepository extends BaseRepository {

	public StationRepository() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static ArrayList<StationSlot> findAllAvailSlotByStationId(int id) throws SQLException{
		ArrayList<StationSlot> s = new ArrayList<StationSlot>();
		String query = "SELECT * FROM slot WHERE status = 0 and station_id = " + Integer.toString(id) + ";";
		ResultSet res = Query(query);
		while(res.next()) {
			StationSlot sl = new StationSlot();
			sl.setBikeId(res.getInt("id"));
			sl.setBikeName(res.getString("Lamborgini"));
			sl.setBikeType(res.getString("Cow"));
			sl.setNumber(res.getInt("number"));
			sl.setSlotId(res.getInt("number"));
			sl.setStationID(res.getInt("station_id"));
			sl.setStatus(res.getInt("status"));
			
			s.add(sl);
		}
		return s;
	}
	
	public static ArrayList<Station> findAllAvailabiltyStation() throws SQLException{
		ArrayList<Station> stationList = new ArrayList<Station>();
		String query = "SELECT * FROM station WHERE number_blank > 0;";
		
		ResultSet res = Query(query);
		while(res.next()) {
			Station s = new Station();
			s.setAddress(res.getString("address"));
			s.setId(res.getInt("id"));
			s.setName(res.getString("name"));
			s.setNumOfBike(res.getInt("number_bike"));
			s.setNumOfBlank(res.getInt("number_blank"));
			s.setNumOfEBike(res.getInt("number_ebike"));
			s.setNumOfTwinBike(res.getInt("number_twin_bike"));
			s.setStationSlots(findAllAvailSlotByStationId(s.getId()));
			s.setStatus(res.getInt("status"));
			s.setTotalSlot(res.getInt("total_slot"));
			stationList.add(s);
		}
		return stationList;
	}
	
	public static Station findStationByStationName(String stationName) throws SQLException{
		
		String query = "SELECT * FROM station WHERE name= " + stationName + ";";
		
		ResultSet res = Query(query);
		Station s = new Station();
		if(res.next()) {
			s.setAddress(res.getString("address"));
			s.setId(res.getInt("id"));
			s.setName(res.getString("name"));
			s.setNumOfBike(res.getInt("number_bike"));
			s.setNumOfBlank(res.getInt("number_blank"));
			s.setNumOfEBike(res.getInt("number_ebike"));
			s.setNumOfTwinBike(res.getInt("number_twin_bike"));
			s.setStationSlots(findAllAvailSlotByStationId(s.getId()));
			s.setStatus(res.getInt("status"));
			s.setTotalSlot(res.getInt("total_slot"));
		}
		
		return s;
	}
	
	public static void updateSlotWithStatus(StationSlot slot) throws SQLException{
		String query = "update slot set id = ? number = ?, station_id = ?, status = ?, bike_id = ? where id = ?";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, slot.getSlotId());
        preparedStatement.setInt(2, slot.getNumber());
        preparedStatement.setInt(3, slot.getStationID());
        preparedStatement.setInt(4, slot.getStatus());
        preparedStatement.setInt(5, slot.getBikeId());
        preparedStatement.setInt(6, slot.getSlotId());
        //preparedStatement.setInt(7, station.getNumOfStop());
        
        preparedStatement.execute();
	}
	
	public static void updateStation(Station station)throws SQLException{
		
		String sql = "update station set name = ?, address = ?, number_bike = ?, number_ebike = ?, number_twin_bike = ?, number_blank = ?, number_unavailable = ?, total_slot = ?, status = ? where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, station.getName());
        preparedStatement.setString(2, station.getAddress());
        preparedStatement.setInt(3, station.getNumOfBike());
        preparedStatement.setInt(4, station.getNumOfEBike());
        preparedStatement.setInt(5, station.getNumOfTwinBike());
        preparedStatement.setInt(6, station.getNumOfBlank());
        //preparedStatement.setInt(7, station.getNumOfStop());
        preparedStatement.setInt(8, station.getTotalSlot());
        preparedStatement.setInt(9, station.getStatus());
        preparedStatement.setInt(10, station.getId());
        preparedStatement.execute();
    }
}
