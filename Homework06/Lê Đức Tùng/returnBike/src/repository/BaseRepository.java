package repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controller.db.ConnectionDatabase;

public class BaseRepository {
	protected static Connection connection;
	public BaseRepository() throws SQLException {
		this.connection = ConnectionDatabase.ConnectionData("tkxdpm");
	}
	public Connection getConnection() {
		return this.connection;
	}
	protected static ResultSet Query(String q) throws SQLException {
		Statement s = connection.createStatement();
		return s.executeQuery(q);
		
	}
	
}
