package entity;

import java.util.ArrayList;

import entity.StationSlot;

public class Station {
	private int id;
	private String name;
	private String address;
	private int numOfBike;
	private int numOfEBike;
	private int numOfTwinBike;
	private int numOfBlank;
	private int totalSlot;
	private int status;
	private ArrayList<StationSlot> stationSlots;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNumOfBike() {
		return numOfBike;
	}
	public void setNumOfBike(int numOfBike) {
		this.numOfBike = numOfBike;
	}
	public int getNumOfEBike() {
		return numOfEBike;
	}
	public void setNumOfEBike(int numOfEBike) {
		this.numOfEBike = numOfEBike;
	}
	public int getNumOfTwinBike() {
		return numOfTwinBike;
	}
	public void setNumOfTwinBike(int numOfTwinBike) {
		this.numOfTwinBike = numOfTwinBike;
	}
	public int getNumOfBlank() {
		return numOfBlank;
	}
	public void setNumOfBlank(int numOfBlank) {
		this.numOfBlank = numOfBlank;
	}
	public int getTotalSlot() {
		return totalSlot;
	}
	public void setTotalSlot(int totalSlot) {
		this.totalSlot = totalSlot;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ArrayList<StationSlot> getStationSlots() {
		return stationSlots;
	}
	public void setStationSlots(ArrayList<StationSlot> stationSlots) {
		this.stationSlots = stationSlots;
	}
	
	
}
