package entity;

import java.sql.Date;

public class Bike {
	private int id;
	private String name;
	private String type;
	private int weight;
	private String licensePlate;
	private Date manuafacturingDate;
	private String producer;
	private String cost;
	private String status;
	private int stationId;

	public Bike() {
	}

	public Bike(String name, String type, int weight, String licensePlate, Date manuafacturingDate, String producer,
			String cost, String status, int stationId) {
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
		this.status = status;
		this.stationId = stationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Date getManuafacturingDate() {
		return manuafacturingDate;
	}

	public void setManuafacturingDate(Date manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

}
