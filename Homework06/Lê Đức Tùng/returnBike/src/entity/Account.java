package entity;


public class Account {
	
	
	private static int currentId;
	
	private int id;
	private String role;
	private int status;
	private String username;
	private String password;
	public Account() {
	}
	public Account(String role, int status, String username, String password) {
		this.role = role;
		this.status = status;
		this.username = username;
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public static void changeAccount(int id) {
		currentId = id;
	}
	
	public static int getCurrentAccount() {
		return currentId;
	}
}
