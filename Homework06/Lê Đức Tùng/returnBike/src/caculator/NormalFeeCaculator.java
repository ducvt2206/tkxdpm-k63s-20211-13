package caculator;

import entity.Bike;
import entity.Activity;
import repository.BikeRepository;

import java.sql.Timestamp;
import java.time.LocalDate;


public class NormalFeeCaculator implements MoneyCaculator {
	
	
	
	@Override
	public String caculateRentFee(Activity activity) {
		// TODO Auto-generated method stub
		Timestamp rentTime = activity.getRentTime();
		Timestamp backTime = new Timestamp(System.currentTimeMillis());
		
		String bikeType = "bike";
		long period = SecondToMinute((backTime.getTime()-rentTime.getTime())/1000);
		
		
		
		try {
			bikeType = BikeRepository.findBikeById(activity.getBikeId()).getType();
			if(period < 10) {
				return "0";
			}else {
				if(period <=30) {
					return Long.toString((period/30)*10000);
				}else {
					return Long.toString(10000 + calc_by_bikeType(bikeType,period-30));
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return "0";
	}
	private long SecondToMinute(long second) {
		return second/60;
	}
	
	private long calc_by_bikeType(String type , long period) {
		
		double coeff = 1;
		if(type.equals("bike")) {
			coeff = 1;
		}else {
			coeff = 1.5;
		}
		if(period%15 <= 7) {
			return (long)(coeff*3000*(period/15));
		}else {
			return (long)(coeff*3000*(period/15+1));
		}
	}
	
	@Override
	public String caculateTransferMoney(Activity activity) {
		
		int rentFee = Integer.parseInt(caculateRentFee(activity));
		int deposit = Integer.parseInt(activity.getDepositMoney());
		return Integer.toString(deposit-rentFee);
	}

}
