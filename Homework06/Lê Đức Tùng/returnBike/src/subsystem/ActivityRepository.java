package subsystem;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import common.enums.StatusActivity;

public class ActivityRepository extends BaseRepository {

	public ActivityRepository() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean insertActivity(int id) throws Exception {
		try {
			PreparedStatement stat = connection.prepareStatement(
					"Insert into activity(status, rent_time, back_time, bike_id) values (?, ?, ?, ?) ");
			stat.setString(1, StatusActivity.RENTING.getStatus());
			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			stat.setTimestamp(3, null);
			stat.setInt(4, id);
			stat.execute();
			return true;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
//	public boolean updateActivity(int id, Activity activity) throws Exception {
//		try {
//			PreparedStatement stat = connection.prepareStatement(
//					"Insert into activity(status, rent_time, back_time, bike_id) values (?, ?, ?, ?) ");
//			stat.setString(1, activity.getStatus());
//			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
//			stat.setTimestamp(3, null);
//			stat.setInt(4, activity.getBikeId());
//			stat.execute();
//			return true;
//		} catch (Exception e) {
//			throw new Exception(e.getMessage());
//		}
//	}
}
