package subsystem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.Account;

public class AccountRepository extends BaseRepository {

	public AccountRepository() throws SQLException {
		super();
	}

	public String checkLogin(String username, String password) throws Exception {
		try {
			PreparedStatement stat = connection
					.prepareStatement("Select * from account WHERE username = ? and password = ?");
			stat.setString(1, username);
			stat.setString(2, password);
			ResultSet rs = stat.executeQuery();
			if (rs.next())
				return rs.getString(3);
			return null;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public boolean register(Account account) throws Exception {
		try {
			PreparedStatement stat = connection
					.prepareStatement("Insert into account(status, role, username, password) values (?, ?, ?, ?) ");
			stat.setInt(1, account.getId());
			stat.setString(2, account.getRole());
			stat.setString(3, account.getUsername());
			stat.setString(4, account.getPassword());
			stat.execute();
			return true;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	public boolean checkUser(String username) throws Exception {
		try {
			PreparedStatement stat = connection.prepareStatement("Select * from account WHERE username = ?");
			stat.setString(1, username);
			ResultSet rs = stat.executeQuery();
			if (rs.next())
				return false;
			return true;
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}
}
