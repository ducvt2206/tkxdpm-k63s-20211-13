package controller.register;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import common.enums.Role;
import service.AccountService;
import entity.Account;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import controller.screen.BaseScreenHandler;

public class RegisterController extends BaseScreenHandler implements Initializable {
	private AccountService accountService;
	@FXML
	private CheckBox checkbox;

	@FXML
	private ComboBox<String> comboBox;

	@FXML
	private TextField txtacc;

	@FXML
	private PasswordField txtconfirm;

	@FXML
	private PasswordField txtpass;

	public RegisterController(Stage stage, String screenPath, AccountService accountService) throws IOException {
		super(stage, screenPath);
		this.accountService = accountService;
	}

	public RegisterController(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}

	public void register() throws Exception {
		if (!checkbox.isSelected()) {
			accountService.notice("Vui lòng đồng ý điều khoản sử dụng!");
		} else if (!txtpass.getText().equals(txtconfirm.getText())) {
			accountService.notice("Mật khẩu không đúng!");
		} else if (!accountService.checkUser(txtacc.getText())) {
			accountService.notice("Username đã tồn tại!");
		} else if (txtacc.getText().isBlank()) {
			accountService.notice("Username không được để trống!");
		} else {
			Account account = new Account(comboBox.getValue(), 1, txtacc.getText(), txtpass.getText());
			accountService.register(account);
			accountService.notice("Đăng ký tài khoản thành công!");
			stage.close();
			clear();
		}
	}

	public void clear() {
		checkbox.setSelected(false);
		txtacc.clear();
		txtconfirm.clear();
		txtpass.clear();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ObservableList<String> roles = FXCollections.observableArrayList(Role.ADMIN.getrole(), Role.CUSTOMER.getrole());
		comboBox.setItems(roles);
	}

}
