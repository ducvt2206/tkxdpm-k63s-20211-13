package controller.pureController;

import subsystem.*;
import entity.payment.*;
import utils.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import common.exception.InvalidCardException;
import common.exception.PaymentException;
import common.exception.UnrecognizedException;

public class PaymentController {
	private InterbankInterface interBank;
	private PaymentCard card;
	private CreditCard cardUser;
//	private PaymentCard cardEcoSystem;
	private int transferMoney=0;
	private String contentTransaction;
	private PaymentTransaction transaction;
	
	public PaymentController(int money) {
		interBank = new InterbankSubsystem();
		transferMoney = money;
		cardUser = Configs.CREDIT_CARD;
//		cardEcoSystem = Configs.SYSTEM_CARD;
	}
	
	public PaymentController() {
		cardUser = Configs.CREDIT_CARD;
		interBank  = new InterbankSubsystem();
//		cardEcoSystem = Configs.SYSTEM_CARD;
	}
	public PaymentController setTransactionMoney(int money) {
		
		transferMoney = money;
		return this;
	}
	public PaymentController setUserCard(PaymentCard card) {
		cardUser = (CreditCard)card;
		return this;
	}
	public PaymentController setContentOfTransaction(String content) {
		contentTransaction = content;
		return this;
	}
	private Map<String, String> processPayment() {
		
		Map<String, String> result = new Hashtable<String, String>();
		result.put("RESULT", "PAYMENT FAILED!");
		result.put("IO-ERROR", "");
		result.put("MESSAGE", "");
		try {
			
			if(transferMoney > 0) {
				transaction = interBank.refund((CreditCard)cardUser, transferMoney, contentTransaction);
				
			}else{
				transaction = interBank.payOrder((CreditCard)cardUser, transferMoney, contentTransaction);
			}
			result.put("RESULT", "PAYMENT SUCCESSFUL!");
			result.put("MESSAGE", "You have succesffully paid the order!");
		} catch (PaymentException | UnrecognizedException ex) {
			result.put("MESSAGE", ex.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			result.put("IO-ERROR", e.getMessage());
		}
		return result;
		
	}

	public String getCardNumber() {
		// TODO Auto-generated method stub
		return cardUser.getCardCode();
	}


	public Map<String, String> callToStatusPayment(String content) {
		// TODO Auto-generated method stub
		contentTransaction = content;
		return processPayment();
	}

	public String checkCvvCodeField(String cvv) {
		// TODO Auto-generated method stub
		String status = "SUCCESS";
		if(!cardUser.getCvvCode().equals(cvv)) {
			status = "WRONG";
		}else if(cardUser.getCvvCode().equals("")) {
			status = "EMPTY";
		}
		return status;
	}
	public CreditCard getCardUser() {
		return this.cardUser;
	}
}
