package controller.pureController;

import java.sql.Timestamp;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import caculator.MoneyCaculator;
import caculator.NormalFeeCaculator;
import entity.Account;
import entity.Activity;
import entity.Bike;
import entity.Station;
import repository.AccountRepository;
import repository.ActivityRepository;
import repository.BikeRepository;
import repository.StationRepository;
import utils.Configs;



public class ReturnBikeController {
	
	private Account currentAccount;
	private Activity currentActivity;
	private Bike currentBike;
	
	private PaymentController payment_ctrl;
	private Map<String,String> messages;
	
	private String rentFee;
	
	//bai xe san dung
//	private ArrayList<Station> availStationList;
	
	//tinh tien
	private MoneyCaculator calc;
	
	// ket qua lua chon bai tra
	private Station selectedStation;
	private int selectedSlotNumber = -1;
	
	public ReturnBikeController() throws Exception{
		calc = new NormalFeeCaculator();
		payment_ctrl = new PaymentController();
		
		currentAccount = AccountRepository.findAccountById(Account.getCurrentAccount());
		currentActivity = ActivityRepository.findActivityById(Activity.getCurrentActivityId());
		currentBike = BikeRepository.findBikeById(currentActivity.getBikeId());
//		availStationList = 
		
	}
	public ReturnBikeController(MoneyCaculator calc) throws Exception{
		
		this.calc = calc;
		
		currentAccount = AccountRepository.findAccountById(Account.getCurrentAccount());
		currentActivity = ActivityRepository.findActivityById(Activity.getCurrentActivityId());
		currentBike = BikeRepository.findBikeById(currentActivity.getBikeId());
	}
	
	public String caculateRentFee() {
		
		rentFee = calc.caculateRentFee(currentActivity);
		return rentFee;
	}
	public void setSelectedSlotNumber(int slot) {
		selectedSlotNumber = slot;
	}
	public HashMap<String, Object> getBaseDataField() {
		// TODO Auto-generated method stub
		HashMap<String,Object> m = new HashMap<String,Object>();
		m.put("rent date", currentActivity.getRentTime());
		m.put("deposit", currentActivity.getDepositMoney());
		m.put("license plate", currentBike.getLicensePlate());
		m.put("bike type", currentBike.getType());
		m.put("user name", currentAccount.getUsername());
		m.put("rent fee", caculateRentFee());
		return m;
	}
	
	public String calcRentFee(String string) {
		// TODO Auto-generated method stub
		
		return null;
	}
	public ArrayList<Station> requestToAvailStation() throws SQLException{
		// TODO Auto-generated method stub
		
		return StationRepository.findAllAvailabiltyStation();
	}
	public void setSelectedStation(Station station) {
		
		this.selectedStation = station;
		// TODO Auto-generated method stub
	}
	public String returnBikeStatus() {
		// TODO Auto-generated method stub
		
		String status = "SUCCESS";
		if(selectedStation == null) {
			status = "ERROR-STATION";
		}else if(selectedSlotNumber == -1) {
			status  = "ERROR-SLOT";
		}
		return status;
	}
	public void completeBussiness() {
		// TODO Auto-generated method stub
		currentActivity.setBackTime(new Timestamp(System.currentTimeMillis()));
		caculateRentFee();
		currentBike.setStatus("Free");
		currentBike.setStationId(selectedStation.getId());
		selectedStation.setTotalSlot(selectedStation.getTotalSlot()+1);
	}
	 public Account getUser() {
		 return currentAccount;
	 }
	 public Activity getActivity() {
		 return currentActivity;
	 }
}
