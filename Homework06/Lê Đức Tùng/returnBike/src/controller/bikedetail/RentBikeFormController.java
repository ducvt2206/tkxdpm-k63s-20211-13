package controller.bikedetail;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.ActivityService;
import controller.screen.BaseScreenHandler;

public class RentBikeFormController extends BaseScreenHandler implements Initializable {
	private Bike bike;
	@FXML
	private TextField txttype;

	@FXML
	private TextField txtmoney;

	@FXML
	private TextField txtphone;

	@FXML
	private TextField txtplate;

	@FXML
	private DatePicker txtrentdate;

	@FXML
	private TextField txtuser;

	public RentBikeFormController(Stage stage, String screenPath, Bike bike) throws IOException {
		super(stage, screenPath);
		this.bike = bike;
		setData(this.bike);
	}

	public void setData(Bike bike) {
		this.txtrentdate.setValue(LocalDate.now());
		this.txtmoney.setText("50000");
		this.txtplate.setText(this.bike.getLicensePlate());
		this.txttype.setText(bike.getType());
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	public void payAndRent() throws NumberFormatException, Exception {
		ActivityService a = new ActivityService();
		a.depositAndRentBike(1, Integer.parseInt(txtmoney.getText()), "Thuê xe");
	}
}
