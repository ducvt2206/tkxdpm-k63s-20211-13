package controller.bikedetail;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import entity.Activity;
import entity.Bike;
import entity.Station;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.ActivityService;
import controller.pureController.PaymentController;
import controller.pureController.ReturnBikeController;
import controller.screen.BaseScreenHandler;

public class PaymentResultScreen extends BaseScreenHandler implements Initializable {
	
	ReturnBikeController returnBikeCtrl;
	
	PaymentController paymentCtrl;
	
	@FXML
	private TextField userNameField;

	@FXML
	private TextField activityNumberField;

	@FXML
	private TextField cardCodeField;

	@FXML
	private TextField depositMoneyField;

	@FXML
	private TextField rentalMoneyField;
	
	@FXML
	private TextField refundField;
	
	@FXML
	private TextField returnDateField;
	
	@FXML
	private Button btnHome;

	public PaymentResultScreen(Stage stage, String screenPath, PaymentController payment, ReturnBikeController returnBike) throws IOException {
		super(stage, screenPath);
		returnBikeCtrl = returnBike;
		paymentCtrl = payment;
		
	}
	
	public void setData() {
		String deposit = returnBikeCtrl.getActivity().getDepositMoney();
		String rentalMoney = returnBikeCtrl.caculateRentFee();
		String refund = Integer.toString(Integer.parseInt(deposit) - Integer.parseInt(rentalMoney));
		userNameField.setText(returnBikeCtrl.getUser().getUsername());
		activityNumberField.setText(Integer.toString(returnBikeCtrl.getActivity().getId()));
		cardCodeField.setText(paymentCtrl.getCardNumber());
		depositMoneyField.setText(deposit);
		rentalMoneyField.setText(rentalMoney);
		refundField.setText(refund);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setData();
		btnHome.setOnMouseClicked(e->{
			this.homeScreenHandler.show();
		});
	}

}
