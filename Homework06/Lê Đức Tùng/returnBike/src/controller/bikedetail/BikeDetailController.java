package controller.bikedetail;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import service.BikeService;
import controller.screen.BaseScreenHandler;

public class BikeDetailController extends BaseScreenHandler implements Initializable {
	@FXML
	private GridPane gridPaneInfo;
	@FXML
	private ImageView imageViewBike;
	@FXML
	private Label txtmanuafacturing;
	@FXML
	private Label txtname;
	@FXML
	private Label txtplate;
	@FXML
	private Label txtstatus;
	@FXML
	private Label txttype;
	private BikeService bikeService;
	private Bike bike;

	public BikeDetailController(Stage stage, String screenPath) throws IOException, SQLException {
		super(stage, screenPath);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			this.bikeService = new BikeService();
			this.bike = bikeService.getDetailBike(1);
			setLabelBike(bike);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void rentBike() throws IOException {
		RentBikeFormController rentBikeFormController = new RentBikeFormController(new Stage(),
				"/view/bikedetail/RentBikeForm.fxml", this.bike);
		rentBikeFormController.show();
	}

	public void setLabelBike(Bike bike) {
		txtname.setText(bike.getName());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		txtmanuafacturing.setText(formatter.format(bike.getManuafacturingDate()).toString());
		txtplate.setText(bike.getLicensePlate());
		txtstatus.setText(bike.getStatus());
		txttype.setText(bike.getType());
	}
}
