package controller.bikedetail;

import java.io.IOException;

import java.net.URL;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import java.sql.Timestamp;
import entity.Station;
import entity.StationSlot;

//import entity.Activity;
//import entity.Account;
//import entity.Bike;
//import java.util.Map;
//import entity.payment.PaymentTransaction;
//import repository.AccountRepository;
//import repository.ActivityRepository;
//import repository.BikeRepository;
import repository.StationRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

import caculator.*;

import service.BaseService;
//import utils.Configs;


import controller.screen.BaseScreenHandler;

import controller.pureController.PaymentController;
import controller.pureController.ReturnBikeController;

public class ReturnBikeFormHandler extends BaseScreenHandler implements Initializable {
	
	
//	private Account currentAccount;
//	private Activity currentActivity;
//	private Bike currentBike;
//	
//	
//	private PaymentController payment_ctrl;
//	private Map<String,String> messages;
//	
//	private String rentFee;
//	
//	//bai xe san dung
//	private ArrayList<Station> availStationList;
//	
//	//tinh tien
//	private MoneyCaculator calc;
//	
//	// ket qua lua chon bai tra
//	private String selectedStationName;
//	private int selectedSlotNumber = 0;
	
	private ReturnBikeController returnBikeCtrl;
	private PaymentController paymentCtrl;
	
	
	@FXML
	private TextField bikeTypeField;

	@FXML
	private TextField txtmoney;
	
	@FXML
	private DatePicker txtReturnDate;
	
	@FXML
	private TextField rentFeeField;

	@FXML
	private TextField contentField;

	@FXML
	private TextField txtplate;

	@FXML
	private DatePicker txtrentdate;

	@FXML
	private TextField txtuser;
	
	@FXML
	private ChoiceBox<String> stationChoiceBox;
	
	@FXML 
	private ChoiceBox<String> slotIdChoiceBox;
	
	@FXML
	private Button btnSubmit;
	
	@FXML
	private TextField txtaddress;
	
	@FXML
	private TextField cardNumberField;
	
	@FXML
	private TextField cvvCodeField;
	
	@FXML
	private Label cardStatusLabel;
	
	@FXML
	private Label returnBikeStatus;
	
//	@FXML
//	private TextField contentField;

 	public ReturnBikeFormHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		
		try {
			paymentCtrl = new PaymentController();
			returnBikeCtrl = new ReturnBikeController();
		}catch (Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	public ReturnBikeFormHandler(Stage stage, String screenPath, MoneyCaculator calc) throws IOException {
		super(stage, screenPath);
		
		try {
			paymentCtrl = new PaymentController();
			returnBikeCtrl = new ReturnBikeController(calc);
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	private void setData() {
		HashMap<String,Object> baseDataField = returnBikeCtrl.getBaseDataField();
		this.txtrentdate.setValue((LocalDate)baseDataField.get("rent date"));
		this.txtReturnDate.setValue(LocalDate.now());
		this.txtmoney.setText((String)baseDataField.get("deposit"));
		this.txtplate.setText((String)baseDataField.get("license plate"));
		this.bikeTypeField.setText((String)baseDataField.get("bike type"));
		this.txtuser.setText((String)baseDataField.get("user name"));
		this.rentFeeField.setText(returnBikeCtrl.caculateRentFee());
		
		
		try {
			ArrayList<Station> availStationList;
			availStationList = returnBikeCtrl.requestToAvailStation();
			if(availStationList.size()>0) {
				for(Station station:availStationList) {
					this.stationChoiceBox.getItems().add(station.getName());
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		
		this.cardNumberField.setText(paymentCtrl.getCardNumber());
		
	}
	
	private void setEvent() {
		
		this.stationChoiceBox.setOnAction(e->{
			String stationName = stationChoiceBox.getSelectionModel().getSelectedItem();
			try {
				Station station = StationRepository.findStationByStationName(stationName);
				returnBikeCtrl.setSelectedStation(station);
				txtaddress.setText(station.getAddress());
				
				ArrayList<StationSlot> sls = station.getStationSlots();
				
				this.slotIdChoiceBox.getItems().clear();
				for(StationSlot slot: sls) {
					slotIdChoiceBox.getItems().add(Integer.toString(slot.getNumber()));
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
		});
		this.slotIdChoiceBox.setOnAction(e->{
			returnBikeCtrl.setSelectedSlotNumber(Integer.parseInt(this.slotIdChoiceBox.getSelectionModel().getSelectedItem()));
		});
		
		this.btnSubmit.setOnMouseClicked(e->{
			
			String statusCvvCode = paymentCtrl.checkCvvCodeField(cvvCodeField.getText());
			
			String status = returnBikeCtrl.returnBikeStatus();
			
			if(statusCvvCode.equals("EMPTY")) {
				cardStatusLabel.setText("You must fill to card code");
				
			}else if(statusCvvCode.equals("WRONG")) {
				cardStatusLabel.setText("Wrong security Code");
			}
			
			if(status.equals("ERROR-STATION")){
				returnBikeStatus.setText("Lack of Station Info!!!");
				return;
//				returnBikeStatus.setStyle("fx-color:red");
			}else if(status.equals("ERROR-SLOT")){
				returnBikeStatus.setText("Lack of Slot Info!!!");
				return;
//				returnBikeStatus.setStyle("fx-color:red");
			}
			Map<String,String> messages  = paymentCtrl.callToStatusPayment(contentField.getText());
			
			if(messages.get("RESULT").equals("PAYMENT SUCCESSFUL!")) {
				try {
					returnBikeCtrl.completeBussiness();
					PaymentResultScreen resultScreen = new PaymentResultScreen(this.stage,"/view/bikedetail/PaymentResult.fxml", paymentCtrl, returnBikeCtrl);
					resultScreen.setScreenTitle("Payment Result");
					resultScreen.show();
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else {
				new BaseService().notice("ERROR! " + messages.get("MESSAGE"));
			}
			
		});
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setData();
		setEvent();
	}
	
}
	