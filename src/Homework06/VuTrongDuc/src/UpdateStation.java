package src;

import javafx.application.Application;
import javafx.stage.Stage;
import controller.LoginController;

public class UpdateStation extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
//            Parent root = FXMLLoader.load(getClass().getResource("/view/fxml/update_station_1.fxml"));
//            Scene scene = new Scene(root);
//            primaryStage.setScene(scene);
//            primaryStage.show();
//            UpdateStationHandler updateStationHandler = new UpdateStationHandler(primaryStage, "/view/fxml/update_station.fxml");
//            updateStationHandler.setScreenTitle("Update");
//            updateStationHandler.show();

//            StationDetailScreenHandler stationDetailScreenHandler = new StationDetailScreenHandler(primaryStage, "/view/fxml/station_detail.fxml" );
//            stationDetailScreenHandler.setScreenTitle("Detail");
//            stationDetailScreenHandler.show();

//            AdminHomeHandler adminHomeHandler = new AdminHomeHandler(primaryStage, "/view/fxml/admin_home.fxml");
//            adminHomeHandler.setScreenTitle("Admin");
//            adminHomeHandler.show();

            LoginController loginScreenHandler = new LoginController(primaryStage, "/view/login.fxml");
            loginScreenHandler.show();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
