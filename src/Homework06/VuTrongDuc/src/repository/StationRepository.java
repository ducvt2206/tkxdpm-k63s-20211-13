package repository;

import entity.Station;
import repository.BaseRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StationRepository extends BaseRepository {
    public StationRepository(){
        super();
    }

    public Station getStationById(int stationId){
        try {
            String sql = "select * from station where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Station station = new Station();
                station.setId(resultSet.getInt(1));
                station.setName(resultSet.getString(2));
                station.setAddress(resultSet.getString(3));
                station.setNumOfBike(resultSet.getInt(4));
                station.setNumOfEBike(resultSet.getInt(5));
                station.setNumOfTwinBike(resultSet.getInt(6));
                station.setNumOfBlank(resultSet.getInt(7));
                station.setTotalSlot(resultSet.getInt(8));
                station.setStatus(resultSet.getInt(9));
                station.setNumOfStop(resultSet.getInt(10));
                return station;
            } else {
                return null;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }


    public void updateStation(Station station){
        try {
            String sql = "update station set name = ?, address = ?, number_bike = ?, number_ebike = ?, number_twin_bike = ?, number_blank = ?, number_unavailable = ?, total_slot = ?, status = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, station.getName());
            preparedStatement.setString(2, station.getAddress());
            preparedStatement.setInt(3, station.getNumOfBike());
            preparedStatement.setInt(4, station.getNumOfEBike());
            preparedStatement.setInt(5, station.getNumOfTwinBike());
            preparedStatement.setInt(6, station.getNumOfBlank());
            preparedStatement.setInt(7, station.getNumOfStop());
            preparedStatement.setInt(8, station.getTotalSlot());
            preparedStatement.setInt(9, station.getStatus());
            preparedStatement.setInt(10, station.getId());
            preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public List<Station> getStationList() {
        try {
            String sql = "select * from station";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Station> stations = new ArrayList<>();

            while(resultSet.next()) {
                Station station = new Station();
                station.setId(resultSet.getInt(1));
                station.setName(resultSet.getString(2));
                station.setAddress(resultSet.getString(3));
                station.setNumOfBike(resultSet.getInt(4));
                station.setNumOfEBike(resultSet.getInt(5));
                station.setNumOfTwinBike(resultSet.getInt(6));
                station.setNumOfBlank(resultSet.getInt(7));
                station.setTotalSlot(resultSet.getInt(8));
                station.setStatus(resultSet.getInt(9));

                stations.add(station);
            }
            return stations;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Station getStationByName(String name){
        try {
            String sql = "select * from station where name = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Station station = new Station();
                station.setId(resultSet.getInt(1));
                station.setName(resultSet.getString(2));
                station.setAddress(resultSet.getString(3));
                station.setNumOfBike(resultSet.getInt(4));
                station.setNumOfEBike(resultSet.getInt(5));
                station.setNumOfTwinBike(resultSet.getInt(6));
                station.setNumOfBlank(resultSet.getInt(7));
                station.setTotalSlot(resultSet.getInt(8));
                station.setStatus(resultSet.getInt(9));
                station.setNumOfStop(resultSet.getInt(10));
                return station;
            } else {
                return null;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

}
