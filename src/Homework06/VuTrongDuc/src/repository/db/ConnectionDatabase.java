package repository.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {
    private static String databaseName = "tkxdpm";
    private static Connection connection;
    public ConnectionDatabase(){
    }
    public static synchronized Connection getConnection(){
        if(connection == null){
            connection = connectionData();
        }
        return connection;
    }
    private static Connection connectionData() {
        Connection connection = null;
        String url = "jdbc:mysql://localhost:3306/" + databaseName;
        String username = "root";
        String password = "Trongduc2206";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected Successful!");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
//    public static void main(String[] args) throws SQLException {
//        Connection con = ConnectionData("tkxdpm");
//        System.out.println(con);
//    }
}
