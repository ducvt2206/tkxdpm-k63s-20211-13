package service;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class BaseService {

	public BaseService() {
	}

	public void notice(String s) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Thông Báo");
		alert.setHeaderText(null);
		alert.setContentText(s);
		alert.showAndWait();
	}
}
