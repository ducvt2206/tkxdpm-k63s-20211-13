package service;

import entity.Station;
import entity.StationSlot;
import javafx.scene.control.Alert;
import repository.SlotRepository;
import repository.StationRepository;

import java.util.*;

public class StationService extends BaseService {
    StationRepository stationRepository;
    SlotRepository slotRepository;
    public StationService(){
        stationRepository = new StationRepository();
        slotRepository = new SlotRepository();
    }

    public Station getStationById(int stationId){
        return stationRepository.getStationById(stationId);
    }

    public List<StationSlot> getStationSlotTable(int stationId){
        List<StationSlot> stationSlots = slotRepository.getStationSlot(stationId);
        return sortBySlotNumber(stationSlots);
    }

    public List<StationSlot> sortBySlotNumber(List<StationSlot> stationSlots){
        Map<StationSlot, Integer> sortedList = new HashMap<>();
        for(StationSlot stationSlot : stationSlots){
            sortedList.put(stationSlot, stationSlot.getNumber());
        }
        Set<Map.Entry<StationSlot, Integer>> entries = sortedList.entrySet();
        Comparator<Map.Entry<StationSlot, Integer>> comparator = new Comparator<Map.Entry<StationSlot, Integer>>() {
            @Override
            public int compare(Map.Entry<StationSlot, Integer> o1, Map.Entry<StationSlot, Integer> o2) {

                return o1.getValue()> o2.getValue()?1:-1;
            }
        };
        List<Map.Entry<StationSlot, Integer>> entryList = new ArrayList<>(entries);
        Collections.sort(entryList, comparator);
        List<StationSlot> stationSlotList = new ArrayList<>();
        for(Map.Entry<StationSlot, Integer> entry: entryList){
            stationSlotList.add(entry.getKey());
        }
        return stationSlotList;
    }
    public void alertError(String s){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Lỗi");
        alert.setHeaderText(null);
        alert.setContentText(s);
        alert.showAndWait();
    }

    public void updateStation(Station station, List<StationSlot> stationSlots){
        stationRepository.updateStation(station);
        for(StationSlot stationSlot : stationSlots){
            int statusToUpdate;
            if(stationSlot.getStatus().equals("Trống")){
                statusToUpdate = 0;
            } else if(stationSlot.getStatus().equals("Dừng hoạt động")){
                statusToUpdate = -1;
            } else {
                statusToUpdate = 1;
            }
            slotRepository.updateStationSlotStatus(stationSlot.getSlotId(), statusToUpdate);
        }
    }

    public boolean checkStationNameExist(String name){
        Station station = stationRepository.getStationByName(name);
        if(station!=null){
            return true;
        } else {
            return false;
        }
    }

    public boolean validateStation(String name, String currentName, String address){
        if(!validateName(name)){
            alertError("Tên bãi xe không hợp lệ");
            return false;
        }
        if(!checkDuplicateName(name, currentName)){
            alertError("Tên bãi xe mới đã tồn tại");
            return  false;
        }
        if(!validateAddress(address)){
            alertError("Địa chỉ không hợp lệ");
            return false;
        }
        return true;
    }
    public boolean validateName(String name){
        if(name == null || name.isEmpty() ){
            return false;
        }
        if(!name.matches("^[a-zA-Z0-9\\s]*$")) {
            return false;
        }
        return true;
    }
    public boolean checkDuplicateName(String name, String currentName){
        if(checkStationNameExist(name) && !name.equals(currentName)){
            return false;
        }
        return true;
    }
    public boolean validateAddress(String address){
        if(address == null || address.isEmpty()){
            return false;
        }
        if(!address.matches("^[a-zA-Z0-9\\s]*$")) {
            return false;
        }
        return true;
    }
}
