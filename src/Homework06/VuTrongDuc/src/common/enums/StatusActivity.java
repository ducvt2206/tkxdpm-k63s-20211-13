package common.enums;

public enum StatusActivity {
	RENTING("đang thuê"), RETURNED("đã trả");

	private String status;

	private StatusActivity(String status) {
		this.setStatus(status);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
