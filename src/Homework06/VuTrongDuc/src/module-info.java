/**
 *
 */
/**
 * @author chukun09
 *
 */
module FinalProject {
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.media;
	requires javafx.base;
	requires javafx.swing;
	requires javafx.fxml;
	requires java.sql;
	requires org.junit.jupiter.api;
	requires org.junit.jupiter.params;
	requires de.jensd.fx.glyphs.fontawesome;
	requires de.jensd.fx.glyphs.commons;
	requires java.net.http;

	opens src to javafx.graphics;
	opens controller to javafx.fxml;
	opens service to javafx.graphics, javafx.fxml, de.jensd.fx.glyphs.fontawesome;
	opens entity to javafx.base;
}

