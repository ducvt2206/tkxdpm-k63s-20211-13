package controller;


import common.enums.Role;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import service.AccountService;

import java.io.IOException;
import java.sql.SQLException;

public class LoginController extends BaseController {
    @FXML
    PasswordField password;
    @FXML
    TextField username;
    AccountService accountService;

    public LoginController(Stage stage, String screenPath) throws IOException, SQLException {
        super(stage, screenPath);
        this.accountService = new AccountService();
    }

    public void login() throws Exception {
        String role = accountService.checkLogin(username.getText(), password.getText());
        if (role.equals(Role.ADMIN.getrole())) {
//            BikeDetailController bikeDetailController = new BikeDetailController(stage, "/view/bikedetail/BikeDetailPage.fxml");
//            bikeDetailController.show();
            AdminHomeController adminHomeController = new AdminHomeController(stage, "/view/admin_home.fxml");
            adminHomeController.show();
        } else {
            accountService.notice("Sai tên đăng nhập hoặc mật khẩu!");
        }
    }

//    public void openRegisterPage() throws Exception {
//        try {
//            RegisterController registerPage = new RegisterController(new Stage(), "/view/login/RegisterPage.fxml",
//                    accountService);
//            registerPage.show();
//        } catch (Exception ex) {
//            throw new Exception(ex.getMessage());
//        }
//    }
}
