package controller;

//import controller.BaseController;

import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Hashtable;

public class BaseController extends FXMLScreenController {

	private Scene scene;
	private BaseController prev;
	protected final Stage stage;
//	protected HomeScreenHandler homeScreenHandler;
	protected Hashtable<String, String> messages;
//	private BaseController bController;

	private BaseController(String screenPath) throws IOException {
		super(screenPath);
		this.stage = new Stage();
	}

	public void setPreviousScreen(BaseController prev) {
		this.prev = prev;
	}

	public BaseController getPreviousScreen() {
		return this.prev;
	}

	public BaseController(Stage stage, String screenPath) throws IOException {
		super(screenPath);
		this.stage = stage;
	}

	public void show() {
		if (this.scene == null) {
			this.scene = new Scene(this.content);
		}
		this.stage.setScene(this.scene);
		this.stage.show();
	}

	public void setScreenTitle(String string) {
		this.stage.setTitle(string);
	}

//	public void setBController(BaseController bController){
//		this.bController = bController;
//	}
//
//	public BaseController getBController(){
//		return this.bController;
//	}
//
//	public void forward(Hashtable messages) {
//		this.messages = messages;
//	}
//
//	public void setHomeScreenHandler(HomeScreenHandler HomeScreenHandler) {
//		this.homeScreenHandler = HomeScreenHandler;
//	}

}
