package service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ValidateNameTest {
    private StationService stationService;
    @BeforeEach
    void setUp(){
        stationService = new StationService();
    }
    @ParameterizedTest
    @CsvSource({
            "station1, true",
            "station22*, false",
            ", false"
    })

    void validateName(String name, boolean expected) {
        boolean rs = stationService.validateName(name);
        assertEquals(expected, rs);
    }


}
