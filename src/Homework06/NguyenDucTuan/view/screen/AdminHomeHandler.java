package controller;

import entity.Station;
import entity.StationSlot;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.util.Callback;
import repository.StationRepository;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AdminHomeHandler extends BaseScreenHandler implements Initializable {

    @FXML
    TableView table;

    @FXML
    TableColumn<Station, Integer> stationIdCol;

    @FXML
    TableColumn<Station, String> stationNameCol;

    @FXML
    TableColumn<Station, String> stationAddressCol;

    @FXML
    TableColumn<Station, String> stationStatusCol;

    @FXML
    TableColumn<Station, Integer> totalBikeCol;

    @FXML
    TableColumn<Station, Integer> blankSlotCol;

    @FXML
    TableColumn<Station, Void> actionCol;

    @FXML
    Button addBikeButton;

    @FXML
    TextField searchTextField;

    StationRepository stationRepository;

    public AdminHomeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.stationRepository = new StationRepository();

        List<Station> stations = stationRepository.getStationList();

        table.setRowFactory( tv -> {
            TableRow<Station> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Station rowData = row.getItem();
                    System.out.println(rowData.getId());
                }

                if (event.getClickCount() == 1 && (! row.isEmpty())) {
                    Station rowData = row.getItem();
                    if (rowData.getNumOfBlank() > 0) {
                        addBikeButton.setStyle("-fx-background-color: #ffa500; ");
                    } else {
                        addBikeButton.setStyle("-fx-background-color: #808080; ");
                    }
                }
            });

            return row ;
        });

//        addBikeButton.setOnAction(event -> );
        stationIdCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        stationNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        stationAddressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        stationStatusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        blankSlotCol.setCellValueFactory(new PropertyValueFactory<>("numOfBlank"));
        Callback<TableColumn<Station, Void>, TableCell<Station, Void>> actionCellFactory = new Callback<TableColumn<Station, Void>, TableCell<Station, Void>>() {
            @Override
            public TableCell<Station, Void> call(final TableColumn<Station, Void> param) {
                final TableCell<Station, Void> cell = new TableCell<Station, Void>() {
                    private final Button btn = new Button();
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        btn.setText("Cập nhật");
                        if(!empty) {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        Callback<TableColumn<Station, Integer>, TableCell<Station, Integer>> totalBikeCellFactory =
                new Callback<TableColumn<Station, Integer>, TableCell<Station, Integer>>() {
            @Override
            public TableCell<Station, Integer> call(TableColumn<Station, Integer> param) {
                final TableCell<Station, Integer> cell = new TableCell<Station, Integer>() {
                    private final Text txt = new Text();

                    @Override
                    public void updateItem(Integer item, boolean empty) {
                        super.updateItem(item, empty);
                        if(!empty) {
                            int total = 0;
//                            System.out.println(getIndex());
//                            System.out.println(getTableView().getItems().size());
                            Station s = getTableView().getItems().get(getIndex());
                            total = s.getNumOfBike() + s.getNumOfEBike() + s.getNumOfTwinBike();
                            txt.setText(String.valueOf(total));
                            setGraphic(txt);
                        }
                    }

                };
                return cell;
            }
        };
        ObservableList<Station> stationsOL = FXCollections.observableList(stations);
//        table.setItems(stationsOL);
        FilteredList<Station> flStation = new FilteredList(stationsOL, p -> true);//Pass the data to a filtered list

        table.setItems(flStation);//Set the table's items using the filtered list

        actionCol.setCellFactory(actionCellFactory);
        totalBikeCol.setCellFactory(totalBikeCellFactory);
        searchTextField.textProperty().addListener((obs, oldValue, newValue) -> {
            flStation.setPredicate(p -> p.getName().toLowerCase().contains(newValue.toLowerCase().trim()));
        });
    }
}
