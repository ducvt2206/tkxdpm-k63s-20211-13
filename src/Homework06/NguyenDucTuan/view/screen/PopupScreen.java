package controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.SpinnerValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PopupScreen implements Initializable {
    @FXML
    Label message;

    @Override
    public void initialize(URL location, ResourceBundle resources){
      message.setText("Success");
    }

}
