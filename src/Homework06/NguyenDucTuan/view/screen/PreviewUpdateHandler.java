package controller;

import entity.Station;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

//public class PreviewUpdateHandler extends BaseScreenHandler implements Initializable {
public class PreviewUpdateHandler extends BaseScreenHandler  {

    @FXML
    Label nameValue;

    @FXML
    Label addressValue;

    @FXML
    Label statusValue;

    @FXML
    Label slotValue;

    @FXML
    PieChart pieChart;

    Station station;

    public PreviewUpdateHandler(Stage stage, Station station) throws IOException {
        super(stage, "/view/preview_update.fxml");
        this.station = station;
        nameValue.setText(station.getName());
        addressValue.setText(station.getAddress());
        statusValue.setText(String.valueOf(station.getStatus()));
        slotValue.setText(String.valueOf(station.getTotalSlot()));
//        pieChart = new PieChart();
        PieChart.Data bike = new PieChart.Data("Bike", station.getNumOfBike());
        PieChart.Data eBike = new PieChart.Data("EBike", station.getNumOfEBike());
        PieChart.Data twinBike = new PieChart.Data("Twin Bike", station.getNumOfTwinBike());
        PieChart.Data blank = new PieChart.Data("Blank", station.getNumOfBlank());
        pieChart.getData().add(bike);
        pieChart.getData().add(eBike);
        pieChart.getData().add(twinBike);
        pieChart.getData().add(blank);
    }
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        nameValue.setText(station.getName());
//        addressValue.setText(station.getAddress());
//        statusValue.setText(String.valueOf(station.getStatus()));
//        slotValue.setText(String.valueOf(station.getTotalSlot()));
//        pieChart = new PieChart();
//        PieChart.Data bike = new PieChart.Data("Bike", station.getNumOfBike());
//        PieChart.Data eBike = new PieChart.Data("EBike", station.getNumOfEBike());
//        PieChart.Data twinBike = new PieChart.Data("Bike", station.getNumOfTwinBike());
//        PieChart.Data blank = new PieChart.Data("Bike", station.getNumOfBlank());
//        pieChart.getData().add(bike);
//        pieChart.getData().add(eBike);
//        pieChart.getData().add(twinBike);
//        pieChart.getData().add(blank);
//    }
}
