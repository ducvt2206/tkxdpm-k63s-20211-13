package controller;

import entity.Station;
import entity.StationSlot;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import repository.StationRepository;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class UpdateStationHandler extends BaseScreenHandler implements Initializable {
    @FXML
    TextField name;

    @FXML
    ComboBox status;

    @FXML
    TextField address;

    @FXML
    Label totalSlot;

    @FXML
    Label stopSlot;

    @FXML
    Label bike;

    @FXML
    Label twinBike;

    @FXML
    Label eBike;

    @FXML
    Label blank;

    @FXML
    TableView table;

    @FXML
    Button confirmButton;

    @FXML
    TableColumn<StationSlot, Integer> slotIdCol;

    @FXML
    TableColumn<StationSlot, Integer> numberCol;

    @FXML
    TableColumn<StationSlot, String> statusCol;

    @FXML
    TableColumn<StationSlot, String> bikeIdCol;

    @FXML
    TableColumn<StationSlot, String> bikeNameCol;

    @FXML
    TableColumn<StationSlot, String> bikeTypeCol;

    @FXML
    TableColumn<StationSlot, Void> actionCol;

    StationRepository stationRepository;

    Station station;
    public UpdateStationHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }
//    private void handleButtonAction(ActionEvent actionEvent){
//        System.out.println(actionEvent.getSource());
//        actionEvent
//    }
    private Button createButton(TableCell tabelCell){
        Button button = new Button();
        TableRow row = tabelCell.getTableRow();
        System.out.println(row.getItem());
        return button;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources){
        this.stationRepository = new StationRepository();
        String statusValue[] =
                { "Available", "Unavailable"};
        status.setItems(FXCollections.observableArrayList(statusValue));
//        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,50, 10);
        station = stationRepository.getStationById(1);
        List<StationSlot> stationSlotMerge = merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
        station.setStationSlots(stationSlotMerge);
        name.setText(station.getName());
        address.setText(station.getAddress());
        totalSlot.setText(String.valueOf(station.getTotalSlot()));
        if(station.getStatus() == 1){
            status.getSelectionModel().selectFirst();
        } else {
            status.getSelectionModel().select(1);
        }
//        totalSlot.setValueFactory(valueFactory);
        slotIdCol.setCellValueFactory(new PropertyValueFactory<>("slotId"));
        numberCol.setCellValueFactory(new PropertyValueFactory<>("number"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        bikeIdCol.setCellValueFactory(new PropertyValueFactory<>("bikeId"));
        bikeNameCol.setCellValueFactory(new PropertyValueFactory<>("bikeName"));
        bikeTypeCol.setCellValueFactory(new PropertyValueFactory<>("bikeType"));
//        actionCol.setCellValueFactory(new PropertyValueFactory<>("button"));
        Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>> cellFactory = new Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>>() {
            @Override
            public TableCell<StationSlot, Void> call(final TableColumn<StationSlot, Void> param) {
                final TableCell<StationSlot, Void> cell = new TableCell<StationSlot, Void>() {
//                    private TableCell tableCell = this;
//                    private final Button btn = createButton(this);
                    private final Button btn = new Button();


                    {
                        btn.setOnAction((ActionEvent event) -> {
                            StationSlot data = getTableView().getItems().get(getIndex());
                           if(btn.getText().equals("Dừng hoạt động")){
                               stationRepository.updateStationSlotStatus(-1, 1, data.getNumber());
                               List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
                               ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotsNew);
                               table.setItems(stationSlots);
                           } else {
                               stationRepository.updateStationSlotStatus(0, 1, data.getNumber());
                               List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
                               ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotsNew);
                               table.setItems(stationSlots);
                           }
                        });
                    }
//                    public String getStatus(){
//                        return
//                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            StationSlot data = getTableView().getItems().get(getIndex());
                            if(data.getStatus().equals("Trống")) {
                                btn.setText("Dừng hoạt động");
                                setGraphic(btn);
                            } else if(data.getStatus().equals("Dừng hoạt động")){
                                btn.setText("Mở hoạt động");
                                setGraphic(btn);
                            } else {
                                setGraphic(null);
                            }

                        }
                    }
                };
                return cell;
            }
        };
        actionCol.setCellFactory(cellFactory);
//        for(StationSlot stationSlot : stationSlotMerge){
//            if(!stationSlot.getStatus().equals("Có Xe")) {
//                stationSlot.getButton().setOnAction(this::handleButtonAction);
//            }
//        }
        ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotMerge);
        table.setItems(stationSlots);
    }

    public void updateStation() {
        try {
//            Stage stage = new Stage();
//            AnchorPane anchorPane = FXMLLoader.load(getClass().getResource("../fxml/popup.fxml"));
//            Scene scene = new Scene(anchorPane);
//            stage.setScene(scene);
//            stage.show();
            Station station = new Station();
            station.setName(name.getText());
            station.setAddress(address.getText());
            if (status.getValue().equals("Available")) {
                station.setStatus(1);
            } else {
                station.setStatus(0);
            }
//            station.setTotalSlot((Integer) totalSlot.getValue());
            station.setTotalSlot(Integer.valueOf(totalSlot.getText()));
            station.setNumOfBike(1);
            station.setNumOfEBike(1);
            station.setNumOfTwinBike(1);
            station.setNumOfBlank(1);
            Stage newStage = new Stage();
//            newStage.initStyle(StageStyle.UNDECORATED);
            PreviewUpdateHandler previewUpdateHandler = new PreviewUpdateHandler(newStage, station);
            previewUpdateHandler.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<StationSlot> merge(List<StationSlot> workingSlot, List<StationSlot> blankSlot){
        for(StationSlot stationSlot : blankSlot){
            workingSlot.add(stationSlot);
        }
        Collections.sort(workingSlot, new Comparator<StationSlot>() {
            @Override
            public int compare(StationSlot o1, StationSlot o2) {
                if(o1.getNumber()<o2.getNumber()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        return workingSlot;
    }
}
