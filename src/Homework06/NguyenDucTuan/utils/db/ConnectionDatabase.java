package utils.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {
    private static String databaseName = "tkxdpm";
    public static Connection ConnectionData() {
        Connection connection = null;
        String url = "jdbc:mysql://localhost:3306/" + databaseName;
        String username = "root";
        String password = "tuannd";
        try {
//            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("Connected Successful!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
//    public static void main(String[] args) throws SQLException {
//        Connection con = ConnectionData("tkxdpm");
//        System.out.println(con);
//    }
}
