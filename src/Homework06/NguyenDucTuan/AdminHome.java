import javafx.application.Application;
import javafx.stage.Stage;
import controller.AdminHomeHandler;
import controller.UpdateStationHandler;

public class AdminHome extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
//            Parent root = FXMLLoader.load(getClass().getResource("/view/fxml/update_station_1.fxml"));
//            Scene scene = new Scene(root);
//            primaryStage.setScene(scene);
//            primaryStage.show();
            AdminHomeHandler adminHomeHandler = new AdminHomeHandler(primaryStage, "/view/admin_home.fxml");
            adminHomeHandler.setScreenTitle("Admin");
            adminHomeHandler.show();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
