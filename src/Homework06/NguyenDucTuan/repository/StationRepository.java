package repository;

import entity.Station;
import entity.StationSlot;
import javafx.scene.control.Button;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StationRepository extends BaseRepository{
    public StationRepository(){
        super();
    }

    public List<Station> getStationList() {
        try {
            String sql = "select * from station";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Station> stations = new ArrayList<>();

            while(resultSet.next()) {
                Station station = new Station();
                station.setId(resultSet.getInt(1));
                station.setName(resultSet.getString(2));
                station.setAddress(resultSet.getString(3));
                station.setNumOfBike(resultSet.getInt(4));
                station.setNumOfEBike(resultSet.getInt(5));
                station.setNumOfTwinBike(resultSet.getInt(6));
                station.setNumOfBlank(resultSet.getInt(7));
                station.setTotalSlot(resultSet.getInt(8));
                station.setStatus(resultSet.getInt(9));

                stations.add(station);
            }
            return stations;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<StationSlot> getStationWorkingSlot(int stationId){
        try {
            String sql = "select slot.id, slot.number, slot.status, bike.id, bike.name, bike.type from station, bike, slot\n" +
                    "where slot.bike_id = bike.id and slot.station_id = station.id and slot.status = 1 and station.id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<StationSlot> stationSlots = new ArrayList<>();
            while(resultSet.next()) {
//                StationSlot stationSlot = new StationSlot(resultSet.getInt(1), resultSet.getInt(2),
//                        resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6));
                StationSlot stationSlot = new StationSlot();
                stationSlot.setSlotId(resultSet.getInt(1));
                stationSlot.setNumber(resultSet.getInt(2));
                stationSlot.setStatus("Có Xe");
                stationSlot.setBikeId(String.valueOf(resultSet.getInt(4)));
                stationSlot.setBikeName(resultSet.getString(5));
                stationSlot.setBikeType(resultSet.getString(6));
                stationSlots.add(stationSlot);
            }
            return stationSlots;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public List<StationSlot> getStationBlankSlot(int stationId){
        try {
            String sql = "select  slot.id, slot.number, slot.status from station, slot where slot.status != 1 and station.id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<StationSlot> stationSlots = new ArrayList<>();
            while(resultSet.next()) {
//                StationSlot stationSlot = new StationSlot(resultSet.getInt(1), resultSet.getInt(2),
//                        resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6));
                StationSlot stationSlot = new StationSlot();
                stationSlot.setSlotId(resultSet.getInt(1));
                stationSlot.setNumber(resultSet.getInt(2));
                if(resultSet.getInt(3) == 0){
                    stationSlot.setStatus("Trống");
                } else {
                    stationSlot.setStatus("Dừng hoạt động");
                }

                stationSlot.setBikeId("Không có xe");
                stationSlot.setBikeName("Không");
                stationSlot.setBikeType("Không");
                if(resultSet.getInt(3) == 0){
                    stationSlot.setButton(new Button("Dừng hoạt động"));
                } else {
                    stationSlot.setButton(new Button("Mở hoạt động"));
                }
                stationSlots.add(stationSlot);
            }
            return stationSlots;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public Station getStationById(int stationId){
        try {
            String sql = "select * from station where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Station station = new Station();
                station.setId(resultSet.getInt(1));
                station.setName(resultSet.getString(2));
                station.setAddress(resultSet.getString(3));
                station.setNumOfBike(resultSet.getInt(4));
                station.setNumOfEBike(resultSet.getInt(5));
                station.setNumOfTwinBike(resultSet.getInt(6));
                station.setNumOfBlank(resultSet.getInt(7));
                station.setTotalSlot(resultSet.getInt(8));
                station.setStatus(resultSet.getInt(9));
                return station;
            } else {
                return null;
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public void updateStationSlotStatus(int status, int stationId, int number) {
        try {
            String sql = "update slot set status = ? where station_id = ? and number = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, status);
            preparedStatement.setInt(2, stationId);
            preparedStatement.setInt(3, number);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
