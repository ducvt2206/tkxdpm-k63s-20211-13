package repository;

import utils.db.ConnectionDatabase;

import java.sql.Connection;

public class BaseRepository {
    protected Connection connection;
    public BaseRepository(){
            this.connection = ConnectionDatabase.ConnectionData();
    }

    public Connection getConnection() {
        return connection;
    }
}
