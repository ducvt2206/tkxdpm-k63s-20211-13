package service;

import java.sql.SQLException;
import entity.Account;
import repository.AccountRepository;

public class AccountService extends BaseService {
	private AccountRepository accountRepository;

	public AccountService() throws SQLException {
		super();
		this.accountRepository = new AccountRepository();
	}

	public boolean checkLogin(String username, String password) throws Exception {
		return accountRepository.checkLogin(username, password);
	}

	public boolean register(Account account) throws Exception {
		return accountRepository.register(account);
	}

	public boolean checkUser(String username) throws Exception {
		return accountRepository.checkUser(username);
	}

}
