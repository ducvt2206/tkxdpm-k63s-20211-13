package service;

import java.sql.SQLException;
import entity.Bike;
import repository.BikeRepository;

public class BikeService extends BaseService {
	private BikeRepository bikeRepository;

	public BikeService() throws SQLException {
		super();
		this.bikeRepository = new BikeRepository();
	}

	public Bike getDetailBike(int id) throws Exception {
		return bikeRepository.getBikeById(id);
	}
}
