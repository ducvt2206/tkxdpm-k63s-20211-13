package service;

import java.sql.SQLException;
import entity.payment.CreditCard;
import entity.payment.PaymentTransaction;
import repository.ActivityRepository;
import subsystem.InterbankSubsystem;
import utils.Configs;

public class ActivityService extends BaseService {
	private InterbankSubsystem interbankSubsystem;
	private ActivityRepository activityRepository;

	public ActivityService() throws SQLException {
		super();
		interbankSubsystem = new InterbankSubsystem();
		activityRepository = new ActivityRepository();
	}

	public void depositAndRentBike(int id, int amount, String contents) throws Exception {
		CreditCard creditCard = Configs.CREDIT_CARD;
		PaymentTransaction a = interbankSubsystem.payOrder(creditCard, amount, contents);
		System.out.println(a.getErrorCode());
		activityRepository.insertActivity(id);
	}
}
