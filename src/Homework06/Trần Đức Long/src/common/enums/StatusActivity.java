package common.enums;

public enum StatusActivity {
	RENTING("đang thuê"), AVAIABLE("có sẵn"), NOTAVAIABLE("hết xe");

	private String status;

	private StatusActivity(String status) {
		this.setStatus(status);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}