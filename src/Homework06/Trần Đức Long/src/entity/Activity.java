package entity;

import java.sql.Timestamp;

public class Activity {
	private int id;
	private String status;
	private Timestamp rentTime;
	private Timestamp backTime;
	private int bikeId;

	public Activity() {
		super();
	}

	public Activity(int id, String status, Timestamp rentTime, Timestamp backTime, int bikeId) {
		super();
		this.id = id;
		this.status = status;
		this.rentTime = rentTime;
		this.backTime = backTime;
		this.bikeId = bikeId;
	}

	public Activity(String status, Timestamp rentTime, Timestamp backTime, int bikeId) {
		super();
		this.status = status;
		this.rentTime = rentTime;
		this.backTime = backTime;
		this.bikeId = bikeId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getRentTime() {
		return rentTime;
	}

	public void setRentTime(Timestamp rentTime) {
		this.rentTime = rentTime;
	}

	public Timestamp getBackTime() {
		return backTime;
	}

	public void setBackTime(Timestamp backTime) {
		this.backTime = backTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

}
