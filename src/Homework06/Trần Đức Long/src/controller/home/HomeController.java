package controller.home;

import java.io.IOException;
import java.sql.SQLException;
import service.*;
import controller.bikedetail.BikeDetailController;
import controller.register.RegisterController;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import view.screen.BaseScreenHandler;

public class HomeController extends BaseScreenHandler {
	@FXML
	PasswordField txtpassword;
	@FXML
	TextField txttdn;
	AccountService accountService;

	public HomeController(Stage stage, String screenPath) throws IOException, SQLException {
		super(stage, screenPath);
		this.accountService = new AccountService();
	}

	public void login() throws Exception {
		if (accountService.checkLogin(txttdn.getText(), txtpassword.getText())) {
			BikeDetailController bikeDetailController = new BikeDetailController(stage, "/view/bikedetail/BikeDetailPage.fxml");
			bikeDetailController.show();
		} else
			accountService.notice("Sai tên đăng nhập hoặc mật khẩu!");
	}

	public void openRegisterPage() throws Exception {
		try {
			RegisterController registerPage = new RegisterController(new Stage(), "/view/login/RegisterPage.fxml",
					accountService);
			registerPage.show();
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}

}
