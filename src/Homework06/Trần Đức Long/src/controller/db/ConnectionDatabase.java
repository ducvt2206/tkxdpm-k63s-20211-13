package controller.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {
	public static Connection ConnectionData(String name) throws SQLException {
		Connection connection = null;
		String url = "jdbc:mysql://localhost:3306/" + name;
	    String username = "root";
	    String password = "12345678";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	public static void main(String[] args) throws SQLException {
		Connection con = ConnectionData("tkxdpm");
		System.out.println(con);
	}

}
