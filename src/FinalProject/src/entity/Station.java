package entity;

import repository.db.ConnectionDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Station {
    private int id;
    private String name;
    private String address;
    private int numOfBike;
    private int numOfEBike;
    private int numOfTwinBike;
    private int numOfBlank;
    private int numOfStop;
    private int totalSlot;
    private int status;
    private List<StationSlot> stationSlots;

    public List<StationSlot> getStationSlots() {
        return stationSlots;
    }

    public int getNumOfStop() {
        return numOfStop;
    }

    public void setNumOfStop(int numOfStop) {
        this.numOfStop = numOfStop;
    }

    public void setStationSlots(List<StationSlot> stationSlots) {
        this.stationSlots = stationSlots;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumOfBike() {
        return numOfBike;
    }

    public void setNumOfBike(int numOfBike) {
        this.numOfBike = numOfBike;
    }

    public int getNumOfEBike() {
        return numOfEBike;
    }

    public void setNumOfEBike(int numOfEBike) {
        this.numOfEBike = numOfEBike;
    }

    public int getNumOfTwinBike() {
        return numOfTwinBike;
    }

    public void setNumOfTwinBike(int numOfTwinBike) {
        this.numOfTwinBike = numOfTwinBike;
    }

    public int getNumOfBlank() {
        return numOfBlank;
    }

    public void setNumOfBlank(int numOfBlank) {
        this.numOfBlank = numOfBlank;
    }

    public int getTotalSlot() {
        return totalSlot;
    }

    public void setTotalSlot(int totalSlot) {
        this.totalSlot = totalSlot;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public static ArrayList<Station> getListStation(){
        ArrayList<Station> stations = new ArrayList<>();
        try {
            PreparedStatement stat = ConnectionDatabase.getConnection()
                    .prepareStatement("Select * from station");

            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                Station station = new Station();
                station.setId(rs.getInt(1));
                station.setName(rs.getString(2));
                station.setAddress(rs.getString(3));
                station.setNumOfBike(rs.getInt(4));
                station.setNumOfEBike(rs.getInt(5));
                station.setNumOfTwinBike(rs.getInt(6));
                station.setNumOfBlank(rs.getInt(7));
                station.setTotalSlot(rs.getInt(8));
                station.setStatus(rs.getInt(9));
                station.setNumOfStop(rs.getInt(10));
                stations.add(station);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return stations;
    }

    public static ArrayList<Station> searchStation(String input) {
        ArrayList<Station> stations = new ArrayList<>();
        try {
            PreparedStatement stat = ConnectionDatabase.getConnection()
                    .prepareStatement("select * from station where name like N'%"+input+"%' or address like N'%"+input+"%'");

            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                Station station = new Station();
                station.setId(rs.getInt(1));
                station.setName(rs.getString(2));
                station.setAddress(rs.getString(3));
                station.setNumOfBike(rs.getInt(4));
                station.setNumOfEBike(rs.getInt(5));
                station.setNumOfTwinBike(rs.getInt(6));
                station.setNumOfBlank(rs.getInt(7));
                station.setTotalSlot(rs.getInt(8));
                station.setStatus(rs.getInt(9));
                station.setNumOfStop(rs.getInt(10));
                stations.add(station);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return stations;
    }
}
