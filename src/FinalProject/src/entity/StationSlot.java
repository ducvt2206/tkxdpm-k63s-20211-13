package entity;

import javafx.scene.control.Button;

public class StationSlot {
    private int slotId;
    private int number;
    private String status;
    private String bikeId;
    private String bikeName;
    private String bikeType;
    private Button button;
    public StationSlot(int slotId, int number, String status, String bikeId, String bikeName, String bikeType){
        this.slotId = slotId;
        this.number = number;
        this.status = status;
        this.bikeId = bikeId;
        this.bikeName = bikeName;
        this.bikeType = bikeType;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public StationSlot(){

    }
    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int stationId) {
        this.slotId = stationId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBikeId() {
        return bikeId;
    }

    public void setBikeId(String bikeId) {
        this.bikeId = bikeId;
    }

    public String getBikeName() {
        return bikeName;
    }

    public void setBikeName(String bikeName) {
        this.bikeName = bikeName;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }
}
