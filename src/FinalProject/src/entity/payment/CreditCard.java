package entity.payment;


public class CreditCard extends PaymentCard {
	private String cardCode;
	private String owner;
	private String cvvCode;
	private String dateExpired;

	public CreditCard(String cardCode, String owner, String cvvCode, String dateExpired) {
		super();
		this.cardCode = cardCode;
		this.owner = owner;
		this.cvvCode = cvvCode;
		this.dateExpired = dateExpired;
	}
}
