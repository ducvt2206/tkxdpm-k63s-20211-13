package entity;

import repository.db.ConnectionDatabase;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Bike {
	private int id;
	private String name;
	private String type;
	private int weight;
	private String licensePlate;
	private Date manuafacturingDate;
	private String producer;
	private String cost;
	private String status;
	private int stationId;

	public Bike() {
	}

	public Bike(String name, String type, int weight, String licensePlate, Date manuafacturingDate, String producer,
			String cost, String status, int stationId) {
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
		this.status = status;
		this.stationId = stationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Date getManuafacturingDate() {
		return manuafacturingDate;
	}

	public void setManuafacturingDate(Date manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}
	public static ArrayList<Bike> getBikeFromStation(int id) {
		ArrayList<Bike> bikes = new ArrayList<>();
		try {
			PreparedStatement stat = ConnectionDatabase.getConnection()
					.prepareStatement("select * from bike where station_id ="+id);
			ResultSet resultSet = stat.executeQuery();
			while (resultSet.next()) {
				Bike bike = new Bike();
				bike.setId(resultSet.getInt(1));
				bike.setName(resultSet.getString(2));
				bike.setType(resultSet.getString(3));
				bike.setWeight(resultSet.getInt(4));
				bike.setLicensePlate(resultSet.getString(5));
				bike.setManuafacturingDate(resultSet.getDate(6));
				bike.setProducer(resultSet.getString(7));
				bike.setCost(String.valueOf(resultSet.getInt(8)));
				bike.setStatus(resultSet.getString(9));
				bike.setStationId(resultSet.getInt(10));
				bikes.add(bike);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return bikes;
	}

}
