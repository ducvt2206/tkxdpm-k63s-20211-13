package entity;

import java.sql.Timestamp;

public class InformationActivityDTO {
	private int id;
	private String name;
	private String type;
	private String licensePlate;
	private Timestamp rentTime;
	private Timestamp backTime;
	private String depositMoney;
	private String totalMoney;
	private String status;

	public InformationActivityDTO() {
		super();
	}

	public InformationActivityDTO(int id, String name, String type, String licensePlate, Timestamp rentTime,
			Timestamp backTime, String depositMoney, String totalMoney, String status) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.licensePlate = licensePlate;
		this.rentTime = rentTime;
		this.backTime = backTime;
		this.depositMoney = depositMoney;
		this.totalMoney = totalMoney;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Timestamp getRentTime() {
		return rentTime;
	}

	public void setRentTime(Timestamp rentTime) {
		this.rentTime = rentTime;
	}

	public Timestamp getBackTime() {
		return backTime;
	}

	public void setBackTime(Timestamp backTime) {
		this.backTime = backTime;
	}

	public String getDepositMoney() {
		return depositMoney;
	}

	public void setDepositMoney(String depositMoney) {
		this.depositMoney = depositMoney;
	}

	public String getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(String totalMoney) {
		this.totalMoney = totalMoney;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
