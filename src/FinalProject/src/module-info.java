/**
 *
 */
/**
 * @author chukun09
 *
 */
module FinalProject {
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.media;
	requires javafx.base;
	requires javafx.swing;
	requires javafx.fxml;
	requires java.sql;
	requires org.junit.jupiter.api;
	requires org.junit.jupiter.params;
	requires de.jensd.fx.glyphs.fontawesome;
	requires de.jensd.fx.glyphs.commons;
	requires java.net.http;

	requires java.naming;


	opens application to javafx.graphics, javafx.fxml;
	opens controller.screen to javafx.graphics, javafx.fxml;
	opens controller.home to javafx.graphics, javafx.fxml;
	opens controller.register to javafx.graphics, javafx.fxml;
	opens controller.bikedetail to javafx.graphics, javafx.fxml, de.jensd.fx.glyphs.fontawesome;
	opens controller.station to javafx.graphics, javafx.fxml;
	opens service to javafx.graphics, javafx.fxml, de.jensd.fx.glyphs.fontawesome;
	opens utils to java.net.http;
	opens entity to javafx.base;
	opens view.liststation;
	opens view.stationdetails;



}
