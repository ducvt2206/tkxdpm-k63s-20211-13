package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entity.Activity;
import entity.payment.CreditCard;
import entity.payment.PaymentTransaction;
import repository.ActivityRepository;
import repository.IActivityRepository;
import subsystem.InterbankSubsystem;
import utils.Configs;

public class ActivityService extends BaseService implements IActivityService {
	private InterbankSubsystem interbankSubsystem;
	private IActivityRepository iActivityRepository;
	private static ActivityService instance;

	public ActivityService() throws SQLException {
		super();
		interbankSubsystem = new InterbankSubsystem();
		iActivityRepository = new ActivityRepository();
	}

	public static ActivityService getInstance() throws SQLException {
		if (instance == null) {
			instance = new ActivityService();
		}
		return instance;
	}

	public void depositAndRentBike(int id, int amount, String contents) throws Exception {
		CreditCard creditCard = Configs.CREDIT_CARD;
		PaymentTransaction a = interbankSubsystem.payOrder(creditCard, amount, contents);
		if(a.getErrorCode().equals("02")) {
			this.notice("Số dư không đủ vui lòng nạp thêm");
		}
		iActivityRepository.insertActivity(id, Integer.toString(amount));
	}

	public boolean checkUserId(int id) throws Exception {
		return iActivityRepository.checkUserId(id);
	}
//	public List<Activity> getActivityByUserId(int id){
//		List<Activity> list = new ArrayList<Activity>();
//		

//	}
}
