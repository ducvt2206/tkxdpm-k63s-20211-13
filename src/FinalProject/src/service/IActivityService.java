package service;

public interface IActivityService {
	public void depositAndRentBike(int id, int amount, String contents) throws Exception;

	public boolean checkUserId(int id) throws Exception;
}
