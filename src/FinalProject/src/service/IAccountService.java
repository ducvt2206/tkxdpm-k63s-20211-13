package service;

import entity.Account;

public interface IAccountService {
	public String checkLogin(String username, String password) throws Exception;
	public boolean register(Account account) throws Exception;
	public boolean checkUser(String username) throws Exception;
	public String getNameById(int id) throws Exception;
}
