package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import common.enums.TypeBike;
import entity.Bike;
import javafx.scene.image.Image;
import repository.BikeRepository;
import repository.IBikeRepository;

public class BikeService extends BaseService implements IBikeService {
	private IBikeRepository iBikeRepository;
	private static BikeService instance;

	public BikeService() throws SQLException {
		super();
		this.iBikeRepository = new BikeRepository();
	}

	public Bike getDetailBike(int id) throws Exception {
		return iBikeRepository.getBikeById(id);
	}

	public static BikeService getInstance() throws SQLException {
		if (instance == null) {
			instance = new BikeService();
		}
		return instance;
	}

	public String checkDepositMoney(String type) {
		int amount = 0;
		if (type.equals(TypeBike.BIKE.gettype())) {
			amount = 200000;
		} else if (type.equals(TypeBike.TWINBIKE.gettype())) {
			amount = 500000;
		} else
			amount = 1000000;
		return convertStringToCurrency(amount);
	}

	public Image getImage(String type) throws FileNotFoundException {
		String url = "file:asset/xe-dap-dien.png";
		if (type.equals(TypeBike.BIKE.gettype())) {
			url = "file:asset/xe-dap.jpg";
		} else if (type.equals(TypeBike.TWINBIKE.gettype())) {
			url = "file:asset/xe-dap-doi-pax-2r.jpg";
		}
		Image image = new Image(url, true);
		return image;
	}

	public void updateStatusBike(String status, int id) throws Exception {
		iBikeRepository.updateStatusBike(status, id);
	}
//	public String totalFeeRentBike(Bike bike) {
//		
//	}
}
