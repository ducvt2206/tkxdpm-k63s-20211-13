package service;

import java.sql.SQLException;
import entity.Account;
import repository.AccountRepository;
import repository.IAccountRepository;

public class AccountService extends BaseService implements IAccountService {
	private IAccountRepository iAccountRepository;
	private static AccountService instance;

	public AccountService() throws SQLException {
		super();
		this.iAccountRepository = new AccountRepository();
	}

	public static AccountService getInstance() throws SQLException {
		if (instance == null) {
			instance = new AccountService();
		}
		return instance;
	}

	public String checkLogin(String username, String password) throws Exception {
		return iAccountRepository.checkLogin(username, password);
	}

	public boolean register(Account account) throws Exception {
		return iAccountRepository.register(account);
	}

	public boolean checkUser(String username) throws Exception {
		return iAccountRepository.checkUser(username);
	}

	public String getNameById(int id) throws Exception {
		return iAccountRepository.getNameById(id);
	}

}
