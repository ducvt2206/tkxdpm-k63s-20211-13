package service;

import entity.Station;
import entity.StationSlot;

import java.util.List;

public interface IStationService {
     Station getStationById(int stationId);
     List<StationSlot> getStationSlotTable(int stationId);
     List<StationSlot> sortBySlotNumber(List<StationSlot> stationSlots);
     void updateStation(Station station, List<StationSlot> stationSlots);
     boolean validateStation(String name, String currentName, String address);
     void alertError(String s);
}
