package service;

import java.io.FileNotFoundException;
import java.text.ParseException;

import entity.Bike;
import javafx.scene.image.Image;

public interface IBikeService {
	public Bike getDetailBike(int id) throws Exception;

	public String checkDepositMoney(String type);

	public Image getImage(String type) throws FileNotFoundException;

	public void notice(String duplicateUserid);

	public String convertCurrencyToString(String amount) throws ParseException;

	public void updateStatusBike(String status, int id) throws Exception;
}
