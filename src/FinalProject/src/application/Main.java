package application;

import java.io.IOException;
import java.sql.SQLException;
import controller.home.HomeController;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException, SQLException {
		HomeController homeController = new HomeController(primaryStage, "/view/login/LoginPage.fxml");
		homeController.setScreenTitle("Login Page");
		homeController.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
