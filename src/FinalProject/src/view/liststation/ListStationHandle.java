package view.liststation;

import controller.screen.BaseController;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import view.stationdetails.StationDetailsHandle;

import java.io.IOException;
import java.util.ArrayList;

public class ListStationHandle extends BaseController {

	@FXML
	TableView tableView;

	@FXML
	TableColumn<Station, String> name;

	@FXML
	TableColumn<Station, String> address;

	@FXML
	TableColumn<Station, Integer> singleBike;

	@FXML
	TableColumn<Station, Integer> eBike;

	@FXML
	TableColumn<Station, Integer> twinBike;

	@FXML
	TextField searchInput;

	private ObservableList<Station> stationsOb;

	public ListStationHandle(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		searchInput.setOnKeyPressed(keyEvent -> handleSearchEnter(keyEvent));
		tableView.setOnMouseClicked(event -> {
			try {
				if (event.getClickCount() == 2)
					showStationDetails(event);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		initialize();
	}

	public void handleHomeClick(ActionEvent actionEvent) {
	}

	public void handleSearchEnter(KeyEvent keyEvent) {
		if (keyEvent.getCode().equals(KeyCode.ENTER)) {
			String input = searchInput.getText();

			if (!input.trim().isEmpty()) {
				ArrayList<Station> listNewStation = Station.searchStation(input);
				ObservableList<Station> stationsOb = FXCollections.observableArrayList(listNewStation);
				tableView.setItems(stationsOb);
				tableView.refresh();
			}
		}
	}

	public void handleStationSelected(MouseEvent mouseEvent) {
	}

	public void initialize() {

		try {
			ArrayList<Station> stations = Station.getListStation();
			stationsOb = FXCollections.observableArrayList(stations);

			name.setCellValueFactory(new PropertyValueFactory<Station, String>("name"));
			address.setCellValueFactory(new PropertyValueFactory<Station, String>("address"));
			singleBike.setCellValueFactory(new PropertyValueFactory<Station, Integer>("numOfBike"));
			eBike.setCellValueFactory(new PropertyValueFactory<Station, Integer>("numOfEBike"));
			twinBike.setCellValueFactory(new PropertyValueFactory<Station, Integer>("numOfTwinBike"));

			tableView.setItems(stationsOb);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void showStationDetails(MouseEvent mouseEvent) throws IOException {
		Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
		BaseController handler = new StationDetailsHandle(stage, "/view/stationdetails/StationDetails.fxml",
				(Station) tableView.getSelectionModel().getSelectedItem());
		handler.show();
	}

	public void back(ActionEvent event) throws IOException {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/view/liststation/ListStation.fxml"));
		Parent parent = loader.load();
		Scene scene = new Scene(parent);
		stage.setScene(scene);
	}
}
