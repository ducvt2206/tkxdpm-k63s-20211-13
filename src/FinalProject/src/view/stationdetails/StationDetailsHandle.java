package view.stationdetails;

import controller.bikedetail.BikeDetailController;
import controller.screen.BaseController;
import entity.Bike;
import entity.Station;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import view.liststation.ListStationHandle;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;

public class StationDetailsHandle extends BaseController {
	@FXML
	Label stationId;

	@FXML
	Label name;

	@FXML
	Label address;

	@FXML
	Label numberOfSingleBike;

	@FXML
	Label numberOfTwinBike;

	@FXML
	Label numberOfEBike;

	@FXML
	Label numberOfEmpty;

	@FXML
	private TableView<Bike> tableView;

	@FXML
	TableColumn<Bike, Integer> barcode;

	@FXML
	TableColumn<Bike, String> bikeName;

	@FXML
	TableColumn<Bike, String> type;

	@FXML
	TableColumn<Bike, String> licensePlate;

	@FXML
	TableColumn<Bike, String> producer;

	@FXML
	ImageView imgStation;

	@FXML
	private Button backButton;

	private Station station;

	private ObservableList<Bike> bikesOb;

	public StationDetailsHandle(Stage stage, String screenPath, Station station) throws IOException {
		super(stage, screenPath);
		this.station = station;
		backButton.setOnMouseClicked(event -> {
			try {
				back(event);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		tableView.setOnMouseClicked(event -> {
			try {
				if (event.getClickCount() == 2) {
					showBikeDetails(event);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		setData(station);
	}

	public void showBikeDetails(MouseEvent event) throws Exception {
		BikeDetailController bikeDetailController = new BikeDetailController(stage,
				"/view/bikedetail/BikeDetailPage.fxml", tableView.getSelectionModel().getSelectedItem().getId());
		bikeDetailController.setPreviousScreen(this);
		bikeDetailController.show();
	}

	public void setData(Station station) {
		stationId.setText(String.valueOf(station.getId()));
		name.setText(station.getName());
		address.setText(station.getAddress());
		numberOfSingleBike.setText(String.valueOf(station.getNumOfBike()));
		numberOfTwinBike.setText(String.valueOf(station.getNumOfTwinBike()));
		numberOfEBike.setText(String.valueOf(station.getNumOfEBike()));
		numberOfEmpty.setText(String.valueOf(station.getNumOfBlank()));
		File file = new File("C:\\Users\\ducma\\IdeaProjects\\Ecobike\\src\\Img\\station.jpg");
		Image image = null;
		try {
			image = new Image(file.toURI().toURL().toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		imgStation.setImage(image);

		try {
			ArrayList<Bike> bikes = Bike.getBikeFromStation(station.getId());
			bikesOb = FXCollections.observableArrayList(bikes);
			barcode.setCellValueFactory(new PropertyValueFactory<Bike, Integer>("id"));
			bikeName.setCellValueFactory(new PropertyValueFactory<Bike, String>("name"));
			type.setCellValueFactory(new PropertyValueFactory<Bike, String>("type"));
			licensePlate.setCellValueFactory(new PropertyValueFactory<Bike, String>("licensePlate"));
			producer.setCellValueFactory(new PropertyValueFactory<Bike, String>("producer"));
			tableView.setItems(bikesOb);
		} catch (Exception e) {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText("Danh sách rỗng.");
			alert.show();
		}
	}

	public void back(MouseEvent event) throws IOException {
		Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		BaseController home = new ListStationHandle(stage, "/view/liststation/ListStation.fxml");
		home.setScreenTitle("List Station");
		home.show();

	}
}
