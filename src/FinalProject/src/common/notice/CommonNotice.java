package common.notice;

public class CommonNotice {
	public static final String BIKE_NOT_AVAILABEL = "Xe không khả dụng hoặc đã được thuê";
	public static final String INVALID_LOGIN = "Vui lòng kiểm tra lại tên đăng nhập hoặc mật khẩu";
	public static final String NOT_ACCEPT_RULE ="Vui lòng đồng ý điều khoản sử dụng";
	public static final String PASSWORD_NOT_MATCH = "Mật khẩu không trùng khớp";
	public static final String USERNAME_EMPTY = "Tên đăng nhập không được để trống";
	public static final String DUPLICATE_USERNAME = "Tên đăng nhập đã tồn tại";
	public static final String REGISTER_SUCCESSFUL = "Đăng ký thành công";
	public static final String DUPLICATE_USERID= "Người dùng đã thuê xe, vui lòng trả xe trước khi thuê lần tiếp theo";
}
