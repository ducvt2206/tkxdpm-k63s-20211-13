package common.enums;

public enum StatusBike {

	RENTING("Đang thuê"), AVAILABLE("Có sẵn"), UNAVAILABEL("Không khả dụng");

	private String status;

	private StatusBike(String status) {
		this.setstatus(status);
	}

	public String getstatus() {
		return status;
	}

	public void setstatus(String status) {
		this.status = status;
	}
}
