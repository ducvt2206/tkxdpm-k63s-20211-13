package common.enums;

public enum TypeBike {
	BIKE("Xe đạp"), TWINBIKE("Xe đạp đôi"), EBIKE("Xe đạp điện");

	private String type;

	private TypeBike(String type) {
		this.settype(type);
	}

	public String gettype() {
		return type;
	}

	public void settype(String type) {
		this.type = type;
	}
}
