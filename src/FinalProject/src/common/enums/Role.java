package common.enums;

public enum Role {

	ADMIN("Quản trị viên"), CUSTOMER("Khách hàng");

	private String role;

	private Role(String role) {
		this.setrole(role);
	}

	public String getrole() {
		return role;
	}

	public void setrole(String role) {
		this.role = role;
	}
}
