package controller.home;

import java.io.IOException;
import java.sql.SQLException;

import common.enums.Role;
import common.notice.CommonNotice;
import service.*;
import controller.bikedetail.BikeDetailController;
import controller.register.RegisterController;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import controller.screen.BaseController;
import view.liststation.ListStationHandle;

public class HomeController extends BaseController {
	@FXML
	PasswordField txtpassword;
	@FXML
	TextField txttdn;
	AccountService accountService;

	public HomeController(Stage stage, String screenPath) throws IOException, SQLException {
		super(stage, screenPath);
		this.accountService = new AccountService();
	}

	public void login() throws Exception {
		String role = accountService.checkLogin(txttdn.getText(), txtpassword.getText());
		if (role == null) {
			accountService.notice(CommonNotice.INVALID_LOGIN);
		} else if (role.equals(Role.CUSTOMER.getrole())) {
			BaseController controller = new ListStationHandle(stage,"/view/liststation/ListStation.fxml");
			controller.show();
		} else if (role.equals(Role.ADMIN.getrole())) {
			AdminHomeController adminHomeController = new AdminHomeController(stage, "/view/admin_home.fxml");
			adminHomeController.show();
		}

	}

	public void openRegisterPage() throws Exception {
		try {
			RegisterController registerPage = new RegisterController(new Stage(), "/view/login/RegisterPage.fxml",
					accountService);
			registerPage.show();
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}

}
