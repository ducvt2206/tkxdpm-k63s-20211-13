package controller.bikedetail;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import common.enums.StatusBike;
import common.notice.CommonNotice;
import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import service.ActivityService;
import service.BikeService;
import service.IActivityService;
import service.IBikeService;
import utils.Configs;
import controller.screen.BaseController;

public class BikeDetailController extends BaseController implements Initializable {
	@FXML
	private GridPane gridPaneInfo;
	@FXML
	private ImageView imageViewBike;
	@FXML
	private Label txtmanuafacturing;
	@FXML
	private Label txtname;
	@FXML
	private Label txtplate;
	@FXML
	private Label txtstatus;
	@FXML
	private Label txttype;
	private IBikeService iBikeService;
	private IActivityService iActivityService;
	private Bike bike;
	private int id;

	public BikeDetailController(Stage stage, String screenPath, int id) throws Exception {
		super(stage, screenPath);
		this.id = id;
		this.iBikeService = BikeService.getInstance();
		this.iActivityService = ActivityService.getInstance();
		this.bike = iBikeService.getDetailBike(id);
		this.bike.setId(id);
		Image image = iBikeService.getImage(bike.getType());
		imageViewBike.setImage(image);
		setLabelBike(bike);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void rentBike() throws Exception {
		String status = bike.getStatus();
		if (iActivityService.checkUserId(Configs.userId)) {
			iBikeService.notice(CommonNotice.DUPLICATE_USERID);
		} else if (status.equals(StatusBike.AVAILABLE.getstatus())) {
			RentBikeFormController rentBikeFormController = new RentBikeFormController(new Stage(),
					"/view/bikedetail/RentBikeForm.fxml", this.bike, this.iBikeService);
			rentBikeFormController.show();
		} else
			iBikeService.notice(CommonNotice.BIKE_NOT_AVAILABEL);

	}

	public void backPage() {
		this.getPreviousScreen().show();
	}

	public void setLabelBike(Bike bike) {
		txtname.setText(bike.getName());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		txtmanuafacturing.setText(formatter.format(bike.getManuafacturingDate()).toString());
		txtplate.setText(bike.getLicensePlate());
		txtstatus.setText(bike.getStatus());
		txttype.setText(bike.getType());
	}
}
