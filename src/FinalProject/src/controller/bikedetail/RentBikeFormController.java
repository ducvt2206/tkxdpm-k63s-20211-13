package controller.bikedetail;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import common.enums.StatusBike;
import service.*;
import entity.Bike;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.Configs;
import controller.screen.BaseController;

public class RentBikeFormController extends BaseController implements Initializable {
	private Bike bike;
	@FXML
	private TextField txttype;

	@FXML
	private TextField txtmoney;

	@FXML
	private TextField txtphone;

	@FXML
	private TextField txtplate;

	@FXML
	private DatePicker txtrentdate;

	@FXML
	private TextField txtuser;
	private IBikeService iBikeService;
	private IAccountService iAccountService;

	public RentBikeFormController(Stage stage, String screenPath, Bike bike, IBikeService iBikeService)
			throws Exception {
		super(stage, screenPath);
		this.bike = bike;
		this.iAccountService = new AccountService();
		this.iBikeService = iBikeService;
		setData(this.bike);
	}

	public void setData(Bike bike) throws Exception {
		this.txtrentdate.setValue(LocalDate.now());
		this.txtmoney.setText(iBikeService.checkDepositMoney(bike.getType()));
		this.txtplate.setText(this.bike.getLicensePlate());
		this.txttype.setText(bike.getType());
		this.txtuser.setText(iAccountService.getNameById(Configs.userId));
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	public void payAndRent() throws NumberFormatException, Exception {
		IActivityService a = ActivityService.getInstance();
		a.depositAndRentBike(bike.getId(), Integer.parseInt(iBikeService.convertCurrencyToString(txtmoney.getText())),
				"Thuê xe");
		iBikeService.updateStatusBike(StatusBike.RENTING.toString(), bike.getId());
		this.stage.close();
	}
}
