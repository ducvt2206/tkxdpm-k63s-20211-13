package controller.station;

import controller.screen.BaseController;
import entity.Station;
import entity.StationSlot;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import service.StationService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class StationDetailController extends BaseController implements Initializable {
    @FXML
    Label nameValue;

    @FXML
    Label statusValue;

    @FXML
    Label addressValue;

    @FXML
    Label totalSlot;

    @FXML
    Label stopSlot;

    @FXML
    Label bike;

    @FXML
    Label twinBike;

    @FXML
    Label eBike;

    @FXML
    Label blank;

    @FXML
    Label activeSlot;

    @FXML
    TableView table;

    @FXML
    Button addBikeButton;

    @FXML
    Button updateButton;

    @FXML
    Label homeLabel;

    @FXML
    TableColumn<StationSlot, Integer> slotIdCol;

    @FXML
    TableColumn<StationSlot, Integer> numberCol;

    @FXML
    TableColumn<StationSlot, String> statusCol;

    @FXML
    TableColumn<StationSlot, String> bikeIdCol;

    @FXML
    TableColumn<StationSlot, String> bikeNameCol;

    @FXML
    TableColumn<StationSlot, String> bikeTypeCol;

    @FXML
    TableColumn<StationSlot, Void> actionCol;

//    StationRepository stationRepository;

    StationService stationService;

    Station station;

    List<StationSlot> stationSlotTable;

    int stationId;
    public StationDetailController(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    public StationDetailController(Stage stage, String screenPath, int stationId) throws IOException {
        super(stage, screenPath);
        this.stationId = stationId;
        station = stationService.getStationById(stationId);
        stationSlotTable = stationService.getStationSlotTable(stationId);
        nameValue.setText(station.getName());
        addressValue.setText(station.getAddress());
        station.setStationSlots(stationSlotTable);
        bike.setText(String.valueOf(station.getNumOfBike()));
        twinBike.setText(String.valueOf(station.getNumOfTwinBike()));
        eBike.setText(String.valueOf(station.getNumOfEBike()));
        blank.setText(String.valueOf(station.getNumOfBlank()));
        stopSlot.setText(String.valueOf(station.getNumOfStop()));
        int activeNum = station.getTotalSlot() - station.getNumOfStop();
        activeSlot.setText(String.valueOf(activeNum));
        totalSlot.setText(String.valueOf(station.getTotalSlot()));
        if(station.getStatus() == 1){
            statusValue.setText("Hoạt động");
        } else {
            statusValue.setText("Dừng hoạt động");
        }
        ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotTable);
        table.setItems(stationSlots);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources){
//        this.stationRepository = new StationRepository();
        this.stationService = new StationService();

        homeLabel.setOnMouseClicked(e -> {
            getPreviousScreen().show();
        });
//        String statusValue[] =
//                { "Hoạt động", "Dừng hoạt động"};
//        status.setItems(FXCollections.observableArrayList(statusValue));
//        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,50, 10);
//        station = stationRepository.getStationById(1);
//        station = stationRepository.getStationById(stationId);


//        List<StationSlot> stationSlotMerge = stationService.merge(1);


//        name.setText(station.getName());
//        address.setText(station.getAddress());


//        totalSlot.setValueFactory(valueFactory);
        slotIdCol.setCellValueFactory(new PropertyValueFactory<>("slotId"));
        numberCol.setCellValueFactory(new PropertyValueFactory<>("number"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        bikeIdCol.setCellValueFactory(new PropertyValueFactory<>("bikeId"));
        bikeNameCol.setCellValueFactory(new PropertyValueFactory<>("bikeName"));
        bikeTypeCol.setCellValueFactory(new PropertyValueFactory<>("bikeType"));
//        actionCol.setCellValueFactory(new PropertyValueFactory<>("button"));
//        Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>> cellFactory = new Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>>() {
//            @Override
//            public TableCell<StationSlot, Void> call(final TableColumn<StationSlot, Void> param) {
//                final TableCell<StationSlot, Void> cell = new TableCell<StationSlot, Void>() {
//                    //                    private TableCell tableCell = this;
////                    private final Button btn = createButton(this);
//                    private final Button btn = new Button();
//
//
//                    {
//                        btn.setOnAction((ActionEvent event) -> {
//                            StationSlot data = getTableView().getItems().get(getIndex());
//                            if(btn.getText().equals("Dừng hoạt động")){
//                                stationRepository.updateStationSlotStatus(-1, 1, data.getNumber());
//                                Station newStation = stationRepository.getStationById(1);
//                                int newStop = newStation.getNumOfStop() + 1;
//                                stationRepository.updateStopSlot(1, newStop);
//                                stopSlot.setText(String.valueOf(newStop));
//                                List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
//                                ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotsNew);
//                                table.setItems(stationSlots);
//                            } else {
//                                stationRepository.updateStationSlotStatus(0, 1, data.getNumber());
//                                Station newStation = stationRepository.getStationById(1);
//                                int newStop = newStation.getNumOfStop() - 1;
//                                stationRepository.updateStopSlot(1, newStop);
//                                stopSlot.setText(String.valueOf(newStop));
//                                List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
//                                ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotsNew);
//                                table.setItems(stationSlots);
//                            }
//                        });
//                    }
//                    //                    public String getStatus(){
////                        return
////                    }
//                    @Override
//                    public void updateItem(Void item, boolean empty) {
//                        super.updateItem(item, empty);
//                        if (empty) {
//                            setGraphic(null);
//                        } else {
//                            StationSlot data = getTableView().getItems().get(getIndex());
//                            if(data.getStatus().equals("Trống")) {
//                                btn.setText("Dừng hoạt động");
//                                setGraphic(btn);
//                            } else if(data.getStatus().equals("Dừng hoạt động")){
//                                btn.setText("Mở hoạt động");
//                                setGraphic(btn);
//                            } else {
//                                setGraphic(null);
//                            }
//
//                        }
//                    }
//                };
//                return cell;
//            }
//        };
//        actionCol.setCellFactory(cellFactory);
//        for(StationSlot stationSlot : stationSlotMerge){
//            if(!stationSlot.getStatus().equals("Có Xe")) {
//                stationSlot.getButton().setOnAction(this::handleButtonAction);
//            }
//        }

    }

    public void update(){
        try {
            UpdateStationController updateStationHandler = new UpdateStationController(stage, "/view/update_station.fxml", station);
            updateStationHandler.setPreviousScreen(this);
            updateStationHandler.show();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

//    public List<StationSlot> merge(List<StationSlot> workingSlot, List<StationSlot> blankSlot){
//        for(StationSlot stationSlot : blankSlot){
//            workingSlot.add(stationSlot);
//        }
//        Collections.sort(workingSlot, new Comparator<StationSlot>() {
//            @Override
//            public int compare(StationSlot o1, StationSlot o2) {
//                if(o1.getNumber()<o2.getNumber()) {
//                    return 1;
//                } else {
//                    return 0;
//                }
//            }
//        });
//        return workingSlot;
//    }
}
