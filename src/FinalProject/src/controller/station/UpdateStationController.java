package controller.station;

import controller.station.PreviewUpdateController;
import controller.screen.BaseController;
import entity.Station;
import entity.StationSlot;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import service.IStationService;
import service.StationService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class UpdateStationController extends BaseController implements Initializable {
    @FXML
    TextField name;

    @FXML
    ComboBox status;

    @FXML
    TextField address;

    @FXML
    Label totalSlot;

    @FXML
    Label stopSlot;

    @FXML
    Label bike;

    @FXML
    Label twinBike;

    @FXML
    Label eBike;

    @FXML
    Label blank;

    @FXML
    Label activeSlot;

    @FXML
    TableView<StationSlot> table;

    @FXML
    Button confirmButton;

    @FXML
    TableColumn<StationSlot, Integer> slotIdCol;

    @FXML
    TableColumn<StationSlot, Integer> numberCol;

    @FXML
    TableColumn<StationSlot, String> statusCol;

    @FXML
    TableColumn<StationSlot, String> bikeIdCol;

    @FXML
    TableColumn<StationSlot, String> bikeNameCol;

    @FXML
    TableColumn<StationSlot, String> bikeTypeCol;

    @FXML
    TableColumn<StationSlot, Void> actionCol;

    @FXML
    Label backLabel;

//    StationRepository stationRepository;

    IStationService stationService;

    Station station;

    List<StationSlot> stationSlotTable ;
    int stationId;
    public UpdateStationController(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    public UpdateStationController(Stage stage, String screenPath, Station station) throws IOException {
        super(stage, screenPath);
        this.station = station;


        stationSlotTable = stationService.getStationSlotTable(station.getId());
        station.setStationSlots(stationSlotTable);
        name.setText(station.getName());
        address.setText(station.getAddress());
        bike.setText(String.valueOf(station.getNumOfBike()));
        twinBike.setText(String.valueOf(station.getNumOfTwinBike()));
        eBike.setText(String.valueOf(station.getNumOfEBike()));
        blank.setText(String.valueOf(station.getNumOfBlank()));
        stopSlot.setText(String.valueOf(station.getNumOfStop()));
        int activeNum = station.getTotalSlot() - station.getNumOfStop();
        activeSlot.setText(String.valueOf(activeNum));
        totalSlot.setText(String.valueOf(station.getTotalSlot()));
        if(station.getStatus() == 1){
            status.getSelectionModel().selectFirst();
        } else {
            status.getSelectionModel().select(1);
        }

        backLabel.setOnMouseClicked(e -> {
            getPreviousScreen().show();
        });
        ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotTable);
        table.setItems(stationSlots);
    }
//    private void handleButtonAction(ActionEvent actionEvent){
//        System.out.println(actionEvent.getSource());
//        actionEvent
//    }

    @Override
    public void initialize(URL location, ResourceBundle resources){
//        this.stationRepository = new StationRepository();
        stationService = new StationService();
//        stationSlotTable = merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
//        stationSlotTable = stationService.merge(station.getId());
        String statusValue[] =
                { "Hoạt động", "Dừng hoạt động"};
        status.setItems(FXCollections.observableArrayList(statusValue));
        status.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if(String.valueOf(newValue).equals("Dừng hoạt động")){
                    for(StationSlot stationSlot : stationSlotTable){
                        stationSlot.setStatus("Dừng hoạt động");
                    }
                    ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotTable);
                    table.setItems(stationSlots);
                    table.refresh();
                }
            }
        });

        slotIdCol.setCellValueFactory(new PropertyValueFactory<>("slotId"));
        numberCol.setCellValueFactory(new PropertyValueFactory<>("number"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("status"));
        bikeIdCol.setCellValueFactory(new PropertyValueFactory<>("bikeId"));
        bikeNameCol.setCellValueFactory(new PropertyValueFactory<>("bikeName"));
        bikeTypeCol.setCellValueFactory(new PropertyValueFactory<>("bikeType"));
        Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>> cellFactory = new Callback<TableColumn<StationSlot, Void>, TableCell<StationSlot, Void>>() {
            @Override
            public TableCell<StationSlot, Void> call(final TableColumn<StationSlot, Void> param) {
                final TableCell<StationSlot, Void> cell = new TableCell<StationSlot, Void>() {
                    private final Button btn = new Button();


                    {
                        btn.setOnAction((ActionEvent event) -> {
                            StationSlot data = getTableView().getItems().get(getIndex());

                           if(btn.getText().equals("Dừng hoạt động")){
//                               stationRepository.updateStationSlotStatus(-1, 1, data.getNumber());
//                               Station newStation = stationRepository.getStationById(1);
                               int newStop = Integer.valueOf(stopSlot.getText()) + 1;
                               int newActive = Integer.valueOf(activeSlot.getText()) - 1;
//                               stationRepository.updateStopSlot(1, newStop);
                               stopSlot.setText(String.valueOf(newStop));
                               activeSlot.setText(String.valueOf(newActive));
                               data.setStatus("Dừng hoạt động");
                               ObservableList<TableColumn<StationSlot,?>> cols = getTableView().getColumns();
                               TableColumn column = cols.get(1);
                               int id = (int) column.getCellData(getIndex());
                               for(StationSlot stationSlot : stationSlotTable){
                                   if(stationSlot.getSlotId() == id){
                                       stationSlot.setStatus("Dừng hoạt động");
                                   }
                               }
//                               List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
                               ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotTable);
                               getTableView().setItems(stationSlots);
                               getTableView().refresh();
//                               table.setItems(stationSlots);
                           } else {
//                               stationRepository.updateStationSlotStatus(0, 1, data.getNumber());
//                               Station newStation = stationRepository.getStationById(1);
                               if(status.getValue().toString().equals("Dừng hoạt động")){
                                   stationService.alertError("Bãi xe đang dừng hoạt động");
                               } else {
                                   int newStop = Integer.valueOf(stopSlot.getText()) - 1;
                                   int newActive = Integer.valueOf(activeSlot.getText()) + 1;
//                               stationRepository.updateStopSlot(1, newStop);
                                   stopSlot.setText(String.valueOf(newStop));
                                   activeSlot.setText(String.valueOf(newActive));
//                               data.setStatus("");
                                   ObservableList<TableColumn<StationSlot, ?>> cols = getTableView().getColumns();
                                   TableColumn column = cols.get(1);
                                   int id = (int) column.getCellData(getIndex());
                                   for (StationSlot stationSlot : stationSlotTable) {
                                       if (stationSlot.getSlotId() == id) {
                                           if(stationSlot.getBikeId()!=null && !stationSlot.getBikeId().equals("Không có xe")){
                                               stationSlot.setStatus("Có xe");
                                           } else {
                                               stationSlot.setStatus("Trống");
                                           }
                                       }
                                   }
//                               List<StationSlot> stationSlotsNew= merge(stationRepository.getStationWorkingSlot(1), stationRepository.getStationBlankSlot(1));
                                   ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotTable);
                                   getTableView().setItems(stationSlots);
                                   getTableView().refresh();
                               }
                           }
                        });
                    }
//                    public String getStatus(){
//                        return
//                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            StationSlot data = getTableView().getItems().get(getIndex());
                            String stationStatus = String.valueOf(status.getValue());
                            boolean checkStatus = data.getStatus().equals("Dừng hoạt động") && !(stationStatus.equals("DỪng hoạt động"));
                            if(!data.getStatus().equals("Dừng hoạt động")) {
                                btn.setText("Dừng hoạt động");
                                btn.setTextFill(Color.RED);
                                setGraphic(btn);
                            } else if(data.getStatus().equals("Dừng hoạt động") ){
                                btn.setText("Mở hoạt động");
                                btn.setTextFill(Color.GREEN);
                                setGraphic(btn);
                            } else {
                                setGraphic(null);
                            }

                        }
                    }
                };
                return cell;
            }
        };
        actionCol.setCellFactory(cellFactory);

//        for(StationSlot stationSlot : stationSlotMerge){
//            if(!stationSlot.getStatus().equals("Có Xe")) {
//                stationSlot.getButton().setOnAction(this::handleButtonAction);
//            }
//        }
//        ObservableList<StationSlot> stationSlots = FXCollections.observableList(stationSlotMerge);
//        table.setItems(stationSlots);
    }
//    public boolean validateStation(){
//        String nameToValidate = name.getText();
//        if(nameToValidate == null || nameToValidate.isEmpty() ){
//            stationService.alertError("Tên bãi xe không hợp lệ");
//            return false;
//        }
//        if(stationService.checkStationNameExist(nameToValidate) && !nameToValidate.equals(station.getName())){
//            stationService.alertError("Tên bãi xe mới đã tồn tại");
//            return false;
//        }
//        if(address.getText() == null || address.getText().isEmpty()){
//            stationService.alertError("Địa chỉ không hợp lệ");
//            return false;
//        }
//        return true;
//    }

    public void updateStation() {
        try {
            if(!stationService.validateStation(name.getText(), station.getName(), address.getText())){
                return ;
            }
            Station newStation = new Station();
            newStation.setId(this.station.getId());
            newStation.setName(name.getText());
            newStation.setAddress(address.getText());
            if (status.getValue().equals("Hoạt động")) {
                newStation.setStatus(1);
            } else {
                newStation.setStatus(0);
            }
//            station.setTotalSlot((Integer) totalSlot.getValue());
            newStation.setTotalSlot(Integer.valueOf(totalSlot.getText()));
            newStation.setNumOfBike(Integer.valueOf(bike.getText()));
            newStation.setNumOfEBike(Integer.valueOf(eBike.getText()));
            newStation.setNumOfTwinBike(Integer.valueOf(twinBike.getText()));
            newStation.setNumOfBlank(Integer.valueOf(blank.getText()));
            newStation.setNumOfStop(Integer.valueOf(stopSlot.getText()));
            Stage newStage = new Stage();
//            newStage.initStyle(StageStyle.UNDECORATED);
            PreviewUpdateController previewUpdateController = new PreviewUpdateController(newStage, this.stage, newStation, stationSlotTable);
            previewUpdateController.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
