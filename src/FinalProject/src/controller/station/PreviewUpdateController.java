package controller.station;

import controller.home.AdminHomeController;
import controller.screen.BaseController;
import entity.Station;
import entity.StationSlot;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import service.StationService;

import java.io.IOException;
import java.util.List;

//public class PreviewUpdateHandler extends BaseScreenHandler implements Initializable {
public class PreviewUpdateController extends BaseController {

    @FXML
    Label nameValue;

    @FXML
    Label addressValue;

    @FXML
    Label statusValue;

    @FXML
    Label slotValue;

    @FXML
    PieChart pieChart;

    @FXML
    Button rejectButton;

    @FXML
     Button confirmButton;

    Stage afterUpdateStage;

    Station station;

    List<StationSlot> stationSlots;

//    StationRepository stationRepository = new StationRepository();

    StationService stationService = new StationService();

    public PreviewUpdateController(Stage stage, Stage afterUpdateStage, Station station, List<StationSlot> stationSlots) throws IOException {
        super(stage, "/view/preview_update.fxml");
        this.afterUpdateStage = afterUpdateStage;
        this.station = station;
        this.stationSlots = stationSlots;
        nameValue.setText(station.getName());
        addressValue.setText(station.getAddress());
        if(station.getStatus() == 0){
            statusValue.setText("Dừng hoạt động");
        } else {
            statusValue.setText("Hoạt động");
        }
        slotValue.setText(String.valueOf(station.getTotalSlot()));
//        pieChart = new PieChart();
        PieChart.Data bike = new PieChart.Data("Bike", station.getNumOfBike());
        PieChart.Data eBike = new PieChart.Data("EBike", station.getNumOfEBike());
        PieChart.Data twinBike = new PieChart.Data("Twin Bike", station.getNumOfTwinBike());
        PieChart.Data blank = new PieChart.Data("Blank", station.getNumOfBlank());
        PieChart.Data stop = new PieChart.Data("Unavailable", station.getNumOfStop());
        pieChart.getData().add(bike);
        pieChart.getData().add(eBike);
        pieChart.getData().add(twinBike);
        pieChart.getData().add(blank);
        pieChart.getData().add(stop);

        EventHandler<ActionEvent> reject = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
               stage.close();
            }
        };

        EventHandler<ActionEvent> update = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
                try {
//                    stationRepository.updateStation(station);
//                    for(StationSlot stationSlot : stationSlots){
//                        int statusToUpdate;
//                        if(stationSlot.getStatus().equals("Trống")){
//                            statusToUpdate = 0;
//                        } else if(stationSlot.getStatus().equals("Dừng hoạt động")){
//                            statusToUpdate = -1;
//                        } else {
//                            statusToUpdate = 1;
//                        }
//                        stationRepository.updateStationSlotStatus(stationSlot.getSlotId(), statusToUpdate);
//                    }
                    stationService.updateStation(station, stationSlots);
                    StationDetailController stationDetailScreenHandler = new StationDetailController(afterUpdateStage, "/view/station_detail.fxml", station.getId());
                    AdminHomeController adminHomeController = new AdminHomeController(afterUpdateStage, "/view/admin_home.fxml");
                    stationDetailScreenHandler.setPreviousScreen(adminHomeController);
                    stationDetailScreenHandler.show();
                    stage.close();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Thông báo");
                    alert.setHeaderText(null);
                    alert. setContentText("Cập nhật bãi xe thành công");
                    alert.showAndWait();
                } catch (IOException exception){
                    exception.printStackTrace();
                }
            }
        };

        rejectButton.setOnAction(reject);
        confirmButton.setOnAction(update);
    }

    private void onReject(){
        this.stage.close();
    }
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        nameValue.setText(station.getName());
//        addressValue.setText(station.getAddress());
//        statusValue.setText(String.valueOf(station.getStatus()));
//        slotValue.setText(String.valueOf(station.getTotalSlot()));
//        pieChart = new PieChart();
//        PieChart.Data bike = new PieChart.Data("Bike", station.getNumOfBike());
//        PieChart.Data eBike = new PieChart.Data("EBike", station.getNumOfEBike());
//        PieChart.Data twinBike = new PieChart.Data("Bike", station.getNumOfTwinBike());
//        PieChart.Data blank = new PieChart.Data("Bike", station.getNumOfBlank());
//        pieChart.getData().add(bike);
//        pieChart.getData().add(eBike);
//        pieChart.getData().add(twinBike);
//        pieChart.getData().add(blank);
//    }
}
