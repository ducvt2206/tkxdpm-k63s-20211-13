package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.Bike;

public class BikeRepository extends BaseRepository implements IBikeRepository {

	public BikeRepository() throws SQLException {
		super();
	}

	public Bike getBikeById(int id) throws Exception {
		Bike bike = new Bike();
		try {
			PreparedStatement stat = connection.prepareStatement("Select * from bike Where id = ? ");
			stat.setInt(1, id);
			ResultSet rs = stat.executeQuery();
			if (rs.next()) {
				bike = new Bike(rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getDate(6),
						rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10));
			}
			return bike;
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}

	public void updateStatusBike(String status, int id) throws Exception {
		try {
			PreparedStatement stat = connection.prepareStatement("Update bike set status = ? where id = ?");
			stat.setString(1, status);
			stat.setInt(2, id);
			stat.executeUpdate();
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}
	}
}
