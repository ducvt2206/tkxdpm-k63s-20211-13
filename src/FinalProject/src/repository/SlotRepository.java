package repository;

import entity.StationSlot;
import repository.BaseRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SlotRepository extends BaseRepository implements ISlotRepository{
    public SlotRepository(){
        super();
    }
    public List<StationSlot> getStationSlot(int stationId){
        try {
            String sql = "select slot.id, slot.number, slot.status, bike_id from station, slot \n" +
                    "                    where  slot.station_id = station.id and station.id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, stationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<StationSlot> stationSlots = new ArrayList<>();
            while(resultSet.next()) {
//                StationSlot stationSlot = new StationSlot(resultSet.getInt(1), resultSet.getInt(2),
//                        resultSet.getInt(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6));
                StationSlot stationSlot = new StationSlot();
                stationSlot.setSlotId(resultSet.getInt(1));
                stationSlot.setNumber(resultSet.getInt(2));
                int status = resultSet.getInt(3);
                if(status == -1){
                    stationSlot.setStatus("Dừng hoạt động");
                } else if(status == 0 ){
                    stationSlot.setStatus("Trống");
                } else {
                    stationSlot.setStatus("Có Xe");
                }
                stationSlot.setBikeId(String.valueOf(resultSet.getInt(4)));
//                stationSlot.setBikeName(resultSet.getString(5));
//                stationSlot.setBikeType(resultSet.getString(6));
                stationSlots.add(stationSlot);
            }

            for(StationSlot stationSlot : stationSlots) {
                if (!stationSlot.getBikeId().equals("0")) {
                    String sql2 = "select * from bike where id = ?";
                    PreparedStatement preparedStatement2 = connection.prepareStatement(sql2);
                    preparedStatement2.setInt(1, Integer.valueOf(stationSlot.getBikeId()));
                    ResultSet resultSet2 = preparedStatement2.executeQuery();
                    if (resultSet2.next()) {
                        stationSlot.setBikeType(resultSet2.getString(3));
                        stationSlot.setBikeName(resultSet2.getString(2));
                        stationSlot.setBikeId(String.valueOf(resultSet2.getInt(1)));
                    }
                } else {
                    stationSlot.setBikeId("Không có xe");
                    stationSlot.setBikeName("Không");
                    stationSlot.setBikeType("Không");
                }
            }
            return stationSlots;
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public void updateStationSlotStatus(int id, int status){
        try {
            String sql = "update slot set status = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, status);
            preparedStatement.setInt(2, id);
//            preparedStatement.setInt(3, number);
            preparedStatement.execute();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
