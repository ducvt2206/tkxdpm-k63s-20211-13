package repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import common.enums.StatusActivity;
import common.enums.StatusBike;
import entity.Activity;
import entity.InformationActivityDTO;
import utils.Configs;

public class ActivityRepository extends BaseRepository implements IActivityRepository {

	public ActivityRepository() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean insertActivity(int id, String amount) throws Exception {
		try {
			PreparedStatement stat = connection.prepareStatement(
					"Insert into activity(status, rent_time, bike_id, account_id, deposit_money) values (?, ?, ?, ?, ?) ");
			stat.setString(1, StatusActivity.RENTING.getStatus());
			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			stat.setInt(3, id);
			stat.setInt(4, Configs.userId);
			stat.setString(5, amount);
			stat.execute();
			return true;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public List<InformationActivityDTO> getAllByUserId(int id) throws Exception {
		List<InformationActivityDTO> list = new ArrayList<InformationActivityDTO>();
		try {
			PreparedStatement stat = connection
					.prepareStatement("Select a.id, b.name ,b.`type` ,b.license_plate , a.rent_time , a.back_time , "
							+ "a.deposit_money , a.total_money ,a.status from activity a left join bike b "
							+ "on a.bike_id  = b.id  where a.account_id = ?");
			stat.setInt(1, id);
			ResultSet rs = stat.executeQuery();
			while (rs.next()) {
				InformationActivityDTO infor = new InformationActivityDTO(rs.getInt(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7),
						rs.getString(8), rs.getString(9));
				list.add(infor);
			}
			System.out.println(list);
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(e.getMessage());
		}
	}

	public boolean checkUserId(int id) throws Exception {
		try {
			PreparedStatement stat = connection
					.prepareStatement("Select * from activity where account_id = ? and status = ? ");
			stat.setInt(1, id);
			stat.setString(2, StatusActivity.RENTING.getStatus());
			ResultSet rs = stat.executeQuery();
			if (rs.next())
				return true;
			return false;
		} catch (Exception ex) {
			throw new Exception(ex.getMessage());
		}

	}

//	public boolean updateActivity(int id, Activity activity) throws Exception {
//		try {
//			PreparedStatement stat = connection.prepareStatement(
//					"Insert into activity(status, rent_time, back_time, bike_id) values (?, ?, ?, ?) ");
//			stat.setString(1, activity.getStatus());
//			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
//			stat.setTimestamp(3, null);
//			stat.setInt(4, activity.getBikeId());
//			stat.execute();
//			return true;
//		} catch (Exception e) {
//			throw new Exception(e.getMessage());
//		}
//	}
}
