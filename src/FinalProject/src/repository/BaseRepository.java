package repository;

import java.sql.Connection;
import java.sql.SQLException;

import repository.db.ConnectionDatabase;

public class BaseRepository {
	protected Connection connection;
	public BaseRepository(){
		connection = ConnectionDatabase.getConnection();
	}
}
