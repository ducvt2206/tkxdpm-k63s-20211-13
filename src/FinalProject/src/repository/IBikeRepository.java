package repository;

import entity.Bike;

public interface IBikeRepository {
	public Bike getBikeById(int id) throws Exception;
	public void updateStatusBike(String status, int id) throws Exception;
}
