package repository;

import java.util.List;

import entity.InformationActivityDTO;

public interface IActivityRepository {
	public boolean insertActivity(int id, String amount) throws Exception;

	public List<InformationActivityDTO> getAllByUserId(int id) throws Exception;

	public boolean checkUserId(int id) throws Exception;

}
