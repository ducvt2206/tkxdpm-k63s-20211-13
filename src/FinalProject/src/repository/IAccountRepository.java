package repository;

import entity.Account;

public interface IAccountRepository {
	public String checkLogin(String username, String password) throws Exception;
	public boolean register(Account account) throws Exception;
	public String getNameById(int id) throws Exception;
	public boolean checkUser(String username) throws Exception;
}
