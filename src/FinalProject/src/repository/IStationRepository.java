package repository;

import entity.Station;

import java.util.List;

public interface IStationRepository {
    public Station getStationById(int id);
    public void updateStation(Station station);
    public List<Station> getStationList();
    public Station getStationByName(String name);
}
