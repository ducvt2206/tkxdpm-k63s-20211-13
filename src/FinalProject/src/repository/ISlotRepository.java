package repository;

import entity.StationSlot;

import java.util.List;

public interface ISlotRepository {
    public List<StationSlot> getStationSlot(int stationId);
    public void updateStationSlotStatus(int id, int status);
}
